"use strict";
const {UDemoNetDriver} = require('./Engine/Classes/Engine/DemoNetDriver.js');
const util = require('util');

if (process.argv[2] == undefined){
  console.log("Need a File");
  process.exit(0);
}
let DemoNetDriver = new UDemoNetDriver('.',process.argv[2]);
DemoNetDriver.InitConnection();
DemoNetDriver.ReadPlaybackDemoHeader();
console.log(DemoNetDriver.ReplayStreamer.GetInfo());
