"use strict";
const {Decrypt} = require('./build/Release/FortniteReplayModule');
const fs = require('fs');
const util = require('util');

if (process.argv[2] == undefined){
  console.log("Need a File");
  process.exit(0);
}
let buf = fs.readFileSync(process.argv[2]);
let fileName = process.argv[2] + '_decrypt';

if (process.argv[3] == undefined){
  console.log("Need a key");
  process.exit(0);
}
let key = Number(process.argv[3]);

Decrypt(buf,key);
fs.writeFileSync(fileName,buf);
