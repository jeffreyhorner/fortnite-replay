const fortnite = require('./Game/Fortnite/Fortnite.js');

if (process.argv[2] == undefined){
  console.log("Need a File");
  process.exit(0);
}

console.log(JSON.stringify(fortnite.BasicReplayStatsFromFile(process.argv[2])));
