"use strict"

// Windows Event Logger
var EventLogger = require('node-windows').EventLogger;
var log = new EventLogger('fortnite-replay-service');

const fortnite = require('./Game/Fortnite/Fortnite.js');
const util = require('util')
const http = require('http')
const port = process.env.PORT || 3000

const requestHandler = (request, response) => {

  function BadRequest(msg){
    response.setHeader('Content-Type', 'application/json');
    response.write(JSON.stringify({Error: msg}));
    response.end();
  }

  if (request.method === 'PUT') {
    let body = Buffer.alloc(parseInt(request.headers['content-length']));
    let offset = 0;
    let replay = null;

    request.on('data', chunk => {
      chunk.copy(body,offset);
      offset += chunk.length
    });

    request.on('end', () => {
      if (offset != body.length)
      {
        BadRequest("Content-length doesn't match HTTP body! offset: " + offset + " content-length: " + body.length);
      }
      else 
      {
        try {
          replay = fortnite.BasicReplayStatsFromBuffer(body)
        } catch(err){
          BadRequest(err.toString());
          return;
        }
            
        response.setHeader('Content-Type', 'application/json');
        response.write(JSON.stringify(replay));
        response.end();
      }
    });

  } else {
    BadRequest("Only accept POST");
  }
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
    log.error(`Could not start service ${err}`)
  }

  console.log(`server is listening on ${port}`);
  log.info(`server is listening on ${port}`);
  log.info("we are in " + __dirname);
})
