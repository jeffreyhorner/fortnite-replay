"use strict";
var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'fortnite-replay-service',
  script: require('path').join(__dirname,'http.js')
});

// Listen for the "uninstall" event so we know when it's done.
svc.on('uninstall',function(){
  console.log('Uninstall complete.');
  console.log('The service exists: ',svc.exists);
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  console.log('Starting.');
  svc.start();
});

svc.on('error',function(e){
  console.log(`error: ${e}`);
  process.exit(0);
});

if (process.argv[2] == undefined){
  console.log("Need a command");
  process.exit(0);
}

if (process.argv[2] == 'install'){
  svc.install();
} else if (process.argv[2] == 'uninstall'){
  // Uninstall the service.
  svc.uninstall();
}
