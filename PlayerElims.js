"use strict";
const {UDemoNetDriver} = require('./Engine/Classes/Engine/DemoNetDriver.js');
const {PlayerElimEvent} = require('./Game/Fortnite/PlayerElimEvent.js');
const util = require('util');

if (process.argv[2] == undefined){
  console.log("Need a File");
  process.exit(0);
}
let DemoNetDriver = new UDemoNetDriver('.',process.argv[2]);
DemoNetDriver.InitConnection();
let replayAr = DemoNetDriver.ReplayStreamer.ReplayAr;
let info = DemoNetDriver.ReplayStreamer.Info;
let events = info.Events;
let elims = [];
for (let i = 0; i < events.length; i++){
  if (events[i].Group=='playerElim'){
    let playerElim = new PlayerElimEvent();
    playerElim.out(replayAr,info,events[i]);
    elims.push(playerElim);
  }
}
console.log(JSON.stringify(elims));
