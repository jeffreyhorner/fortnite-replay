"use strict";

const fs = require('fs');
const {FLocalFileNetworkReplayStreamer} = require('../../../NetworkReplayStreaming/LocalFileNetworkReplayStreaming/Public/LocalFileNetworkReplayStreaming.js')
const {FEngineVersion} = require('../../../Core/Public/Misc/EngineVersion.js');
const {TypedVal, FArchive} = require('../../../Core/Public/Serialization/Archive.js');
const {
  FNetGUIDCacheObject,
  UPackageMapClient
} = require('../../../Engine/Classes/Engine/PackageMapClient.js');

const INDEX_NONE = 0xFFFFFFFF;
const MAX_DEMO_READ_WRITE_BUFFER = 1024 * 2;
const ENetworkVersionHistory =
{
  HISTORY_REPLAY_INITIAL          : 1,
  HISTORY_SAVE_ABS_TIME_MS        : 2,      // We now save the abs demo time in ms for each frame (solves accumulation errors)
  HISTORY_INCREASE_BUFFER         : 3,      // Increased buffer size of packets, which invalidates old replays
  HISTORY_SAVE_ENGINE_VERSION       : 4,      // Now saving engine net version + InternalProtocolVersion
  HISTORY_EXTRA_VERSION         : 5,      // We now save engine/game protocol version, checksum, and changelist
  HISTORY_MULTIPLE_LEVELS         : 6,      // Replays support seamless travel between levels
  HISTORY_MULTIPLE_LEVELS_TIME_CHANGES  : 7,      // Save out the time that level changes happen
  HISTORY_DELETED_STARTUP_ACTORS      : 8,      // Save DeletedNetStartupActors inside checkpoints
  HISTORY_HEADER_FLAGS          : 9,      // Save out enum flags with demo header
  HISTORY_LEVEL_STREAMING_FIXES     : 10,     // Optional level streaming fixes.
  HISTORY_SAVE_FULL_ENGINE_VERSION    : 11,     // Now saving the entire FEngineVersion including branch name

  // -----<new versions can be added before this line>-------------------------------------------------
  HISTORY_PLUS_ONE: INDEX_NONE - 1,
  HISTORY_LATEST              : INDEX_NONE
};

const MIN_SUPPORTED_VERSION = ENetworkVersionHistory.HISTORY_EXTRA_VERSION;

const NETWORK_DEMO_MAGIC        = 0x2CF5A13D;
const NETWORK_DEMO_VERSION      = ENetworkVersionHistory.HISTORY_LATEST;
const MIN_NETWORK_DEMO_VERSION    = ENetworkVersionHistory.HISTORY_EXTRA_VERSION;

const NETWORK_DEMO_METADATA_MAGIC   = 0x3D06B24E;
const NETWORK_DEMO_METADATA_VERSION = 0;

class FLevelNameAndTime
{

  constructor(InLevelName, InLevelChangeTimeInMS){
    this.LevelName = new TypedVal('FString','');
    this.LevelChangeTimeInMS = new TypedVal('uint32',0);
  }

  out(Archive)
  {
    Archive.out(this,'LevelName');
    Archive.out(this,'LevelChangeTimeInMS');
  }
}

//enum class EReplayHeaderFlags : uint32
const EReplayHeaderFlags =
{
  None        : 0,
  ClientRecorded    : ( 1 << 0 ),
  HasStreamingFixes : ( 1 << 1 ),
};

class FNetworkDemoHeader
{
  //uint32  Magic;                  // Magic to ensure we're opening the right file.
  //uint32  Version;                // Version number to detect version mismatches.
  //uint32  NetworkChecksum;            // Network checksum
  //uint32  EngineNetworkProtocolVersion;     // Version of the engine internal network format
  //uint32  GameNetworkProtocolVersion;       // Version of the game internal network format

  //DEPRECATED(4.20, "Changelist is deprecated, use EngineVersion.GetChangelist() instead.")
  //uint32  Changelist;               // Engine changelist built from

  //FEngineVersion EngineVersion;         // Full engine version on which the replay was recorded
  //EReplayHeaderFlags HeaderFlags;         // Replay flags
  //TArray<FLevelNameAndTime> LevelNamesAndTimes; // Name and time changes of levels loaded for demo
  //TArray<FString> GameSpecificData;       // Area for subclasses to write stuff
  //
  constructor(){
    this.Magic = new TypedVal('uint32',0);
    this.Version = new TypedVal('uint32',0);
    this.NetworkChecksum = new TypedVal('uint32',0);
    this.EngineNetworkProtocolVersion = new TypedVal('uint32',0);
    this.GameNetworkProtocolVersion = new TypedVal('uint32',0);

    this.Changelist = new TypedVal('uint32',0);

    this.HeaderFlags = new TypedVal('uint32',0);
    this.EngineVersion = new FEngineVersion();
    this.LevelNamesAndTimes = new TypedVal('TArray',[],new FLevelNameAndTime());
    this.GameSpecificData = [];

  }

  out(Ar)
  {
    Ar.out(this,'Magic');

    // Check magic value
    if ( this.Magic != NETWORK_DEMO_MAGIC )
    {
      //UE_LOG( LogDemo, Error, TEXT( "Header.Magic != NETWORK_DEMO_MAGIC" ) );
      throw new Error("Header.Magic != NETWORK_DEMO_MAGIC" );
      Ar.SetError();
      return Ar;
    }

    Ar.out(this,'Version');

    // Check version
    if ( this.Version < MIN_NETWORK_DEMO_VERSION )
    {
      //UE_LOG( LogDemo, Error, TEXT( "Header.Version < MIN_NETWORK_DEMO_VERSION. Header.Version: %i, MIN_NETWORK_DEMO_VERSION: %i" ), Header.Version, MIN_NETWORK_DEMO_VERSION );
      throw new Error("Header.Version < MIN_NETWORK_DEMO_VERSION. Header.Version: " + this.Version + " MIN_NETWORK_DEMO_VERSION: " + MIN_NETWORK_DEMO_VERSION );
      Ar.SetError();
      return Ar;
    }

    Ar.out(this,'NetworkChecksum');
    Ar.out(this,'EngineNetworkProtocolVersion');
    Ar.out(this,'GameNetworkProtocolVersion');

    if (this.Version >= ENetworkVersionHistory.HISTORY_SAVE_FULL_ENGINE_VERSION)
    {
      this.EngineVersion.out(Ar);
    }
    else
    {
      // Previous versions only stored the changelist
      //PRAGMA_DISABLE_DEPRECATION_WARNINGS
      Ar.out(this,'Changelist');

      //if (Ar.IsLoading())
      //{
        // We don't have any valid information except the changelist.
      //  Header.EngineVersion.Set(0, 0, 0, Header.Changelist, FString());
      //}
      //PRAGMA_ENABLE_DEPRECATION_WARNINGS
    }

    if (this.Version < ENetworkVersionHistory.HISTORY_MULTIPLE_LEVELS)
    {
      throw new Error("header 1");
      //FString LevelName;
      //Ar << LevelName;
      //Header.LevelNamesAndTimes.Add(FLevelNameAndTime(LevelName, 0));
    }
    else if (this.Version == ENetworkVersionHistory.HISTORY_MULTIPLE_LEVELS)
    {
      throw new Error("header 2");
      //TArray<FString> LevelNames;
      //Ar << LevelNames;

      //for (const FString& LevelName : LevelNames)
      //{
      //  Header.LevelNamesAndTimes.Add(FLevelNameAndTime(LevelName, 0));
      //}
    }
    else
    {
      //throw new Error("header 3: " + Ar.debugString());
      //Ar << Header.LevelNamesAndTimes;
      Ar.out(this,'LevelNamesAndTimes');
    }

    if (this.Version >= ENetworkVersionHistory.HISTORY_HEADER_FLAGS)
    {
      Ar.out(this,'HeaderFlags');
    }

    //Ar << Header.GameSpecificData;

    return Ar;
  }
}

class DeletedActor
{
  constructor(){
    this.ActorName = new TypedVal('FString','');
  }
  out(Ar){
    Ar.out(this,'ActorName');
  }
}

// We're not good enough yet at running a replay
// so we use this object to capture replay data atm
class UDemoNetObject {
  constructor(){
    this.PacketOffset = new TypedVal('int64',0);
    this.CurrentLevelIndex = new TypedVal('int32',0);
    this.DeletedActors = new TypedVal('TArray',[],new DeletedActor());
    this.GuidCache = null;
  }
}

class UDemoNetDriver
{
  constructor(DemoSavePath,ReplayFileName){
    this.ReplayStreamer = new FLocalFileNetworkReplayStreamer(DemoSavePath,ReplayFileName);
    this.DemoHeader = null;

    this.bHasLevelStreamingFixes = false;
    this.PackageMapClient = null;
    this.DemoNetObject = null;
    this.PlaybackPackets = null;

    this.Packets = [];
    this.ExternalData = []; 
  }

  InitConnection(ReplayBuffer){
    return this.ReplayStreamer.ReadReplay(ReplayBuffer);
  }

  ReadPlaybackDemoHeader(){
    if (this.DemoHeader == null){
      let HeaderAr = this.ReplayStreamer.GetHeaderArchive();
      this.DemoHeader = new FNetworkDemoHeader();
      this.DemoHeader.out(HeaderAr)

      this.bHasLevelStreamingFixes = !!(this.DemoHeader.HeaderFlags & EReplayHeaderFlags.HasStreamingFixes);

    }
  }
  GetDemoHeader(){
    this.ReadPlaybackDemoHeader();
    return this.DemoHeader;
  }

  HasLevelStreamingFixes(){
    return this.bHasLevelStreamingFixes;
  }

  ReadDemoFrameIntoPlaybackPackets(Ar){
    let i = 0; // for indexing
    let bForLevelFastForward = false;

    //console.log("PacketOffset: " + this.DemoNetObject.PacketOffset + ' ' + Ar.Tell());

    //check(!bForLevelFastForward || HasLevelStreamingFixes());

    //if ( Ar.IsError() )
    //{
    //  UE_LOG(LogDemo, Error, TEXT("UDemoNetDriver::ReadDemoFrameIntoPlaybackPackets: Archive Error"));
    //  NotifyDemoPlaybackFailure(EDemoPlayFailure::Generic);
    //  return false;
    //}

    if ( Ar.AtEnd() )
    {
      return false;
    }

    //if ( ReplayStreamer->GetLastError() != ENetworkReplayError::None )
    //{
    //  UE_LOG(LogDemo, Error, TEXT("UDemoNetDriver::ReadDemoFrameIntoPlaybackPackets: ReplayStreamer ERROR: %s"), ENetworkReplayError::ToString(ReplayStreamer->GetLastError()));
    //  NotifyDemoPlaybackFailure(EDemoPlayFailure::Generic);
    //  return false;
   // }

    // Above checks guarantee the Archive is in a valid state, but it's entirely possible that
    // the ReplayStreamer doesn't have more stream data available (i.e., if we only have checkpoint data).
    // Therefore, skip this if we know we're only reading in checkpoint data.
    //if ( !bIsLoadingCheckpoint && !ReplayStreamer->IsDataAvailable() )
    //{
    //  return false;
    //}

    //int32 ReadCurrentLevelIndex = 0;
    let ReadCurrentLevelIndex = {val: new TypedVal('int32',0) };

    if (this.DemoHeader.Version >= ENetworkVersionHistory.HISTORY_MULTIPLE_LEVELS)
    {
      //Ar << ReadCurrentLevelIndex;
      ReadCurrentLevelIndex = Ar.out(ReadCurrentLevelIndex,'val');
      //console.log('ReadCurrentLevelIndex: ' + ReadCurrentLevelIndex);
    }

    //float TimeSeconds = 0.0f;
    let TimeSeconds = {val: new TypedVal('float',0)};

    //Ar << TimeSeconds;
    TimeSeconds = Ar.out(TimeSeconds,'val');
    //console.log('TimeSeconds: ' + TimeSeconds);

    //if (OutTime)
    //{
    //  *OutTime = TimeSeconds;
    //}

    //console.log('DemoHeader.Version: ' + this.DemoHeader.Version + ' ' +ENetworkVersionHistory.HISTORY_LEVEL_STREAMING_FIXES);
    if (this.DemoHeader.Version >= ENetworkVersionHistory.HISTORY_LEVEL_STREAMING_FIXES)
    {
      //DECLARE_SCOPE_CYCLE_COUNTER(TEXT("Demo_ReceiveExports"), Demo_ReceiveExports, STATGROUP_Net);

      //((UPackageMapClient*)ServerConnection->PackageMap)->ReceiveExportData(Ar);
      this.PackageMapClient.ReceiveExportData(Ar);
    }

    // Check to see if we can skip adding these packets.
    // This may happen if the archive isn't set to a proper position due to level fast forwarding.
    //const bool bAppendPackets = bIsLoadingCheckpoint || bForLevelFastForward || LatestReadFrameTime < TimeSeconds;
    let bAppendPackets = true;
    //LatestReadFrameTime = FMath::Max(LatestReadFrameTime, TimeSeconds);
    
    if (this.HasLevelStreamingFixes())
    {
      //uint32 NumStreamingLevels = 0;
      let NumStreamingLevels = {val: new TypedVal('intPacked',0)};

      //Ar.SerializeIntPacked(NumStreamingLevels);
      NumStreamingLevels = Ar.out(NumStreamingLevels,'val')
      //console.log('NumStreamingLevels: ' + NumStreamingLevels);

      // We want to avoid adding the same levels to the Seen list multiple times.
      // This can occur if the Archive is "double read" due to a level fast forward.
      //const bool bAddToSeenList = bAppendPackets && !bForLevelFastForward;
      let bAddToSeenList = true;

      //FString NameTemp;
      for (i = 0; i < NumStreamingLevels; i++)
      {
        let NameTemp = {val: new TypedVal('FString','')};
        //Ar << NameTemp;
        NameTemp = Ar.out(NameTemp,'val');
        //console.log('NameTemp: ' + NameTemp);

        //if (bAddToSeenList)
        //{
          // Add this level to the seen list, but don't actually mark it as being seen.
          // It will be marked when we have processed packets for it.
         // const FLevelStatus& LevelStatus = FindOrAddLevelStatus(NameTemp);
          //SeenLevelStatuses.Add(LevelStatus.LevelIndex);
        //}
      }
    }
    else
    {
      // Read any new streaming levels this frame
      //uint32 NumStreamingLevels = 0;
      let NumStreamingLevels = {val: new TypedVal('uint32',0)};

      //Ar.SerializeIntPacked(NumStreamingLevels);
      NumStreamingLevels = Ar.out(NumStreamingLevels,'val');
      //console.log('NumStreamingLevels: ' + NumStreamingLevels);

      if (NumStreamingLevels > 0){
        console.log("Don't know how to load FTransform's yet!!!!!!!!");
        process.exit();
      }

      for (i = 0; i < NumStreamingLevels; ++i)
      {
        //FString PackageName;
        //FString PackageNameToLoad;
        //FTransform LevelTransform;
        let PackageName = {val: new TypedVal('FString','')};
        let PackageNameToLoad = {val: new TypedVal('FString','')};
        //let LevelTransform = {val: new TypedVal('',)};


        //Ar << PackageName;
        //Ar << PackageNameToLoad;
        //Ar << LevelTransform;
        PackageName = Ar.out(PackageName,'val');
        PackageNameToLoad = Ar.out(PackageNameToLoad,'val');
        //LevelTransform = Ar.out(LevelTransform,'val');
        //console.log('PackageName: ' + PackageName);
        //console.log('PackageNameToLoad: ' + PackageNameToLoad);

        // Don't add if already exists
        //bool bFound = false;
        let bFound = false;

        /*
        for (ULevelStreaming* StreamingLevel : World->GetStreamingLevels())
        {
          FString SrcPackageName = StreamingLevel->GetWorldAssetPackageName();
          FString SrcPackageNameToLoad = StreamingLevel->PackageNameToLoad.ToString();

          if (SrcPackageName == PackageName && SrcPackageNameToLoad == PackageNameToLoad)
          {
            bFound = true;
            break;
          }
        }

        if (bFound)
        {
          continue;
        }

        ULevelStreamingKismet* StreamingLevel = NewObject<ULevelStreamingKismet>(World, NAME_None, RF_NoFlags, nullptr);

        StreamingLevel->SetShouldBeLoaded(true);
        StreamingLevel->SetShouldBeVisible(true);
        StreamingLevel->bShouldBlockOnLoad = false;
        StreamingLevel->bInitiallyLoaded = true;
        StreamingLevel->bInitiallyVisible = true;
        StreamingLevel->LevelTransform = LevelTransform;

        StreamingLevel->PackageNameToLoad = FName(*PackageNameToLoad);
        StreamingLevel->SetWorldAssetByPackageName(FName(*PackageName));

        World->AddStreamingLevel(StreamingLevel);

        UE_LOG(LogDemo, Log, TEXT("ReadDemoFrameIntoPlaybackPackets: Loading streamingLevel: %s, %s"), *PackageName, *PackageNameToLoad);
        */
      }
    }
    
  /*
  #if DEMO_CHECKSUMS == 1
    {
      uint32 ServerDeltaTimeCheksum = 0;
      Ar << ServerDeltaTimeCheksum;

      const uint32 DeltaTimeChecksum = FCrc::MemCrc32(&TimeSeconds, sizeof(TimeSeconds), 0);

      if (DeltaTimeChecksum != ServerDeltaTimeCheksum)
      {
        UE_LOG(LogDemo, Error, TEXT("UDemoNetDriver::ReadDemoFrameIntoPlaybackPackets: DeltaTimeChecksum != ServerDeltaTimeCheksum"));
        NotifyDemoPlaybackFailure(EDemoPlayFailure::Generic);
        return false;
      }
    }
  #endif
   */

    //if (Ar.IsError())
    //{
    //  UE_LOG(LogDemo, Error, TEXT("UDemoNetDriver::ReadDemoFrameIntoPlaybackPackets: Failed to read demo ServerDeltaTime"));
    //  NotifyDemoPlaybackFailure(EDemoPlayFailure::Generic);
    //  return false;
    //}

    //FArchivePos SkipExternalOffset = 0;
    let SkipExternalOffset = {val: new TypedVal('int64',0)};
    if (this.HasLevelStreamingFixes())
    {
      //Ar << SkipExternalOffset;
      SkipExternalOffset = Ar.out(SkipExternalOffset,'val');
      //console.log('SkipExternalOffset: ' + SkipExternalOffset + ' ' + Ar.Tell());
    }

    if (!bForLevelFastForward)
    {
      // Load any custom external data in this frame
      this.LoadExternalData(Ar, TimeSeconds,this.ExternalData);
    }
    else
    {
      Ar.Seek(Ar.Tell() + SkipExternalOffset);
    }

    // Buffer any packets in this frame
    //uint32 SeenLevelIndex = 0;

    {
      //DECLARE_SCOPE_CYCLE_COUNTER(TEXT("Demo_ReadPackets"), Demo_ReadPackets, STATGROUP_Net);

      let idx = 0;
      while (true)
      {
        if (this.HasLevelStreamingFixes())
        {
          let SeenLevelIndex = {val: new TypedVal('intPacked',0)};
          //Ar.SerializeIntPacked(SeenLevelIndex);
          SeenLevelIndex = Ar.out(SeenLevelIndex,'val');
        }

        //int32 PacketBytes = 0;
        //uint8 ReadBuffer[MAX_DEMO_READ_WRITE_BUFFER];
        let PacketBuffer = {buffer: new Buffer.alloc(MAX_DEMO_READ_WRITE_BUFFER), OutBufferSize: new TypedVal('uint32',0), idx: idx};

        if (!this.ReadPacket(Ar, PacketBuffer))
        {
          console.log("UDemoNetDriver::ReadDemoFrameIntoPlaybackPackets: ReadPacket failed.");
          //NotifyDemoPlaybackFailure(EDemoPlayFailure::Generic);
          return false;
        }
        idx++;


        if (PacketBuffer.OutBufferSize == 0)
        {
          break;
        }

        this.Packets.push(PacketBuffer);

        if (!bAppendPackets)
        {
          continue;
        }

        //FPlaybackPacket& Packet = *(new(InPlaybackPackets)FPlaybackPacket);
        //Packet.Data.AddUninitialized(PacketBytes);
        //Packet.TimeSeconds = TimeSeconds;
        //Packet.LevelIndex = ReadCurrentLevelIndex;
        //Packet.SeenLevelIndex = SeenLevelIndex;
        //FMemory::Memcpy(Packet.Data.GetData(), ReadBuffer, PacketBytes);
      }
    }

    return true;
  }

  ReadPacket( Ar, PacketBuffer ) 
  {
   // OutBufferSize = 0;

    //Archive << OutBufferSize;
    PacketBuffer.OutBufferSize = Ar.out(PacketBuffer,'OutBufferSize');
    //console.log('OutBufferSize: ' + PacketBuffer.OutBufferSize);

    if ( Ar.IsError() )
    {
      //UE_LOG( LogDemo, Error, TEXT( "UDemoNetDriver::ReadPacket: Failed to read demo OutBufferSize" ) );
      console.log("UDemoNetDriver::ReadPacket: Failed to read demo OutBufferSize" );
      return false;
    }

    if ( PacketBuffer.OutBufferSize == 0 )
    {
      //console.log('Done Reading Packets');
      return true;		// Done
    }

    if ( PacketBuffer.OutBufferSize > PacketBuffer.buffer.length )
    {
      //UE_LOG( LogDemo, Error, TEXT( "UDemoNetDriver::ReadPacket: OutBufferSize > sizeof( ReadBuffer )" ) );
      console.log("UDemoNetDriver::ReadPacket: OutBufferSize: " + PacketBuffer.OutBufferSize + " > PacketBuffer: " + PacketBuffer.buffer.length);
      return false;
    }

    // Read data from file.
    //Archive.Serialize( OutReadBuffer, OutBufferSize );
    PacketBuffer.buffer = Ar.Buffer.slice(Ar.Tell(),Ar.Tell() + PacketBuffer.OutBufferSize);
    Ar.Seek(Ar.Tell() + PacketBuffer.OutBufferSize);
    //fs.writeFileSync('packet_' + PacketBuffer.idx,PacketBuffer.buffer);

    if ( Ar.IsError() )
    {
      //UE_LOG( LogDemo, Error, TEXT( "UDemoNetDriver::ReadPacket: Failed to read demo file packet" ) );
      console.log("UDemoNetDriver::ReadPacket: Failed to read demo file packet");
      return false;
    }

  /*
  #if DEMO_CHECKSUMS == 1
    {
      uint32 ServerChecksum = 0;
      Archive << ServerChecksum;

      const uint32 Checksum = FCrc::MemCrc32( OutReadBuffer, OutBufferSize, 0 );

      if ( Checksum != ServerChecksum )
      {
        UE_LOG( LogDemo, Error, TEXT( "UDemoNetDriver::ReadPacket: Checksum != ServerChecksum" ) );
        return false;
      }
    }
  #endif
  */

    return true;
  }

  LoadExternalData(Ar, TimeSeconds,ExternalData)
  {
    //DECLARE_SCOPE_CYCLE_COUNTER(TEXT("Demo_LoadExternalData"), Demo_LoadExternalData, STATGROUP_Net);

    while ( true )
    {
     // uint8 ExternalDataBuffer[1024];
      //uint32 ExternalDataNumBits;

      // Read payload into payload/guid map
      //Ar.SerializeIntPacked( ExternalDataNumBits );
      let ExternalDataNumBits = {val: new TypedVal('intPacked',0)};
      ExternalDataNumBits = Ar.out(ExternalDataNumBits,'val');
      //console.log('ExternalDataNumBits: ' + ExternalDataNumBits + ' ' + Ar.Tell());

      if ( ExternalDataNumBits == 0 )
      {
        return;
      } 

      //FNetworkGUID NetGUID;
      let NetGUID = {val: new TypedVal('intPacked',0)};

      // Read net guid this payload belongs to
      //Ar << NetGUID;
      NetGUID = Number(Ar.out(NetGUID,'val'));
      //console.log('NetGUID: ' + NetGUID);
      

      let ExternalDataNumBytes = ( ExternalDataNumBits + 7 ) >> 3;
      //console.log("ExternalDataNumBytes: " + ExternalDataNumBytes);
      let buf = Ar.Buffer.slice(Ar.Tell(),Ar.Tell()+ExternalDataNumBytes);
      Ar.Seek(Ar.Tell() + ExternalDataNumBytes);
      //console.log('After External Data:' + Ar.Tell());

      ExternalData.push({ NetGUID: NetGUID, Data: buf });
     
      //console.log('buf: ' + buf.toString('hex'));

      //Ar.Serialize( ExternalDataBuffer, ExternalDataNumBytes );

      //FBitReader Reader( ExternalDataBuffer, ExternalDataNumBits );

      //Reader.SetEngineNetVer( ServerConnection->EngineNetworkProtocolVersion );
      //Reader.SetGameNetVer( ServerConnection->GameNetworkProtocolVersion );

      //FReplayExternalDataArray& ExternalDataArray = ExternalDataToObjectMap.FindOrAdd( NetGUID );

      //ExternalDataArray.Add( new FReplayExternalData( Reader, TimeSeconds ) );
    }
  }

  LoadCheckpoint(Ar){
    this.DemoNetObject = new UDemoNetObject();

    if (this.HasLevelStreamingFixes()){
      Ar.out(this.DemoNetObject,'PacketOffset');
      //console.log('PacketOffset: ' + this.DemoNetObject.PacketOffset);
    }

    if (this.DemoHeader.Version >= ENetworkVersionHistory.HISTORY_MULTIPLE_LEVELS)
    {
      // *GotoCheckpointArchive << LevelForCheckpoint;
      Ar.out(this.DemoNetObject,'CurrentLevelIndex');
    }
    if ( this.DemoHeader.Version >= ENetworkVersionHistory.HISTORY_DELETED_STARTUP_ACTORS )
    {
      //*GotoCheckpointArchive << DeletedNetStartupActors;
      Ar.out(this.DemoNetObject,'DeletedActors');
    }

    // Load the current guid cache
    
    let NumValues = { val: new TypedVal('int32',0) }
    NumValues = Ar.out(NumValues,'val');

    let GuidCache = [];

    for (let i = 0; i < NumValues; i++){
      let Guid = { val: new TypedVal('intPacked',0)}
      let CacheObject = new FNetGUIDCacheObject();

      Guid = Number(Ar.out(Guid,'val'));
      //console.log('Guid: ' + Guid);
      Ar.out(CacheObject,'OuterGUID');
      Ar.out(CacheObject,'PathName');
      Ar.out(CacheObject,'NetworkChecksum');
      Ar.out(CacheObject,'Flags');
      CacheObject.Guid = Guid;

      GuidCache[Guid] = CacheObject;
      
    }
    this.DemoNetObject.GuidCache = GuidCache;
    //console.log(GuidCache);

    // Save the compatible rep layout map
    //PackageMapClient->SerializeNetFieldExportGroupMap( *CheckpointArchive );
    this.PackageMapClient = new UPackageMapClient();
    this.PackageMapClient.SerializeNetFieldExportGroupMap(Ar);

    this.ReadDemoFrameIntoPlaybackPackets(Ar);
  }
}
exports.FNetworkDemoHeader = FNetworkDemoHeader;
exports.UDemoNetDriver = UDemoNetDriver;
