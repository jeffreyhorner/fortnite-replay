"use strict";

const {TypedVal, FArchive} = require('../../../Core/Public/Serialization/Archive.js');

class FNetGUIDCacheObject
{
  // These fields are set when this guid is static
  constructor(){
    this.Guid = null;
    this.OuterGUID = new TypedVal('intPacked',0);
    this.PathName = new TypedVal('FString','');
    this.NetworkChecksum = new TypedVal('uint32');
    this.Flags = new TypedVal('uint8',0);
  }
}

class FNetFieldExport
{
  constructor() // : bExported( false ), Handle( 0 ), CompatibleChecksum( 0 ), bIncompatible( false )
  {
    this.bExported = false;
    this.Handle = new TypedVal('intPacked',0);
    this.CompatibleChecksum = new TypedVal('uint32',0);
    this.Name = new TypedVal('FString','');
    this.Type = new TypedVal('FString','');
    this.bIncompatible = false;    // If true, we've already determined that this property isn't compatible. We use this to curb warning spam.
  }

  out(Archive)
  {
    let Flags = { val: new TypedVal('uint8',0)};

    Flags = Archive.out(Flags,'val');

    this.bExported = (Flags == 1) ? true : false;

    if ( this.bExported == true)
    {
      Archive.out(this,'Handle');
      Archive.out(this,'CompatibleChecksum');
      this.Name = Archive.out(this,'Name');
      this.Type = Archive.out(this,'Type');
    } else {
      this.Handle = null;
      this.CompatibleChecksum = null;
      this.Name = null;
      this.Type = null;
    }
  }
}


class FNetFieldExportGroup
{
  constructor(){
    this.PathName = new TypedVal('FString','');
    this.PathNameIndex = new TypedVal('intPacked',0);
    this.NetFieldExports = new TypedVal('TArray<IntPacked>',[],new FNetFieldExport());
  }

  out(Archive)
  {
    // Ar << C.PathName;
    Archive.out(this,'PathName');

    //Archive.SerializeIntPacked( C.PathNameIndex );
    Archive.out(this,'PathNameIndex');

    Archive.out(this,'NetFieldExports');
  }
}


class UPackageMapClient {
  constructor(){
    this.NetFieldExportGroup = new TypedVal('TArray',[],new FNetFieldExportGroup());
    this.NetFieldExportGroupPathToIndex = [];
    this.NetFieldExportGroupIndexToPath = [];
    this.NetFieldExportGroupMap = [];
  }

  SerializeNetFieldExportGroupMap( Archive ){
    Archive.out(this,'NetFieldExportGroup')
    for (let i = 0; i < this.NetFieldExportGroup.length; i++){
      let NetFieldExportGroup = this.NetFieldExportGroup[i];
      this.NetFieldExportGroupPathToIndex[NetFieldExportGroup.PathName] = NetFieldExportGroup.PathNameIndex;
      this.NetFieldExportGroupIndexToPath[NetFieldExportGroup.PathNameIndex] = NetFieldExportGroup.PathName;

      // Add the export group to the map
      this.NetFieldExportGroupMap[NetFieldExportGroup.PathName] = NetFieldExportGroup;
    }
  }

  ReceiveExportData(Ar){
	  this.ReceiveNetFieldExports(Ar);
	  this.ReceiveNetExportGUIDs(Ar);
  }
  ReceiveNetFieldExports(Ar)
  {
    let i = 0;
    //check(Connection->InternalAck);

    // Read number of net field exports
    //uint32 NumLayoutCmdExports = 0;
    //Archive.SerializeIntPacked(NumLayoutCmdExports);
    let NumLayoutCmdExports = {val: new TypedVal('intPacked',0)};
    NumLayoutCmdExports = Ar.out(NumLayoutCmdExports,'val');
    //console.log('NumLayoutCmdExports: ' + NumLayoutCmdExports);

    for (i = 0; i < NumLayoutCmdExports; i++)
    {
      //uint32 PathNameIndex = 0;
      //uint32 WasExported = 0;
      //Archive.SerializeIntPacked(PathNameIndex);
      //Archive.SerializeIntPacked(WasExported);
      let PathNameIndex = {var: new TypedVal('intPacked',0)};
      let WasExported = {var: new TypedVal('intPacked',0)};
      PathNameIndex = Ar.out(PathNameIndex);
      //console.log('PathNameIndex: ' + PathNameIndex);
      WasExported = Ar.out(WasExported);
      //console.log('WasExported: ' + WasExported);
      

      //TSharedPtr<FNetFieldExportGroup> NetFieldExportGroup;
      let NetFieldExportGroup = null;
      if (!!WasExported)
      {
        //FString PathName;
        //uint32 NumExports = 0;
        let PathName = {var: new TypedVal('FString','')};
        let NumExports = {var: new TypedVal('intPacked',0)};

        //Archive << PathName;
        //Archive.SerializeIntPacked(NumExports);
        PathName = Ar.out(PathName,'val');
        NumExports = Ar.out(NumExports,'val');
        //console.log('PathName: ' + PathName);
        //console.log('NumExports: ' + NumExports);
        

        //GEngine->NetworkRemapPath(Connection->Driver, PathName, true);
        this.NetFieldExportGroupIndexToPath[PathNameIndex] = PathName;
        this.NetFieldExportGroupPathToIndex[PathName]  = PathNameIndex;

        //NetFieldExportGroup = GuidCache->NetFieldExportGroupMap.FindRef(PathName);
        NetFieldExportGroup = this.NetFieldExportGroupMap[PathName];
        if (NetFieldExportGroup == undefined)
        {
          NetFieldExportGroup = new FNetFieldExportGroup();
          NetFieldExportGroup.PathName = PathName;
          NetFieldExportGroup.PathNameIndex = PathNameIndex;
          NetFieldExportGroup.NetFieldExports = [];
          this.NetFieldExportGroupMap[PathName] = NetFieldExportGroup;
        }
      }
      else
      {
      //  const FString& PathName = GuidCache->NetFieldExportGroupIndexToPath.FindChecked(PathNameIndex);
      //  NetFieldExportGroup = GuidCache->NetFieldExportGroupMap.FindRef(PathName);
        let PathName = this.NetFieldExportGroupIndexToPath[PathNameIndex];
        NetFieldExportGroup = this.NetFieldExportGroupMap[PathName];
      }

      //TArray<FNetFieldExport>& Exports = NetFieldExportGroup->NetFieldExports;
      let Exports = NetFieldExportGroup.NetFieldExports;
      //FNetFieldExport Export;
      //Archive << Export;
 
      let Export = new FNetFieldExport();
      Ar.out(Export);
      //console.log('Export: ' + Export);

      if (Exports.IsValidIndex(Export.Handle))
      {
        Exports[Export.Handle] = Export;
      }
      else
      {
       //UE_LOG(LogNetPackageMap, Error, TEXT("ReceiveNetFieldExports: Invalid NetFieldExportHandle '%i', Max '%i'"), Export.Handle, Exports.Num());
       console.log("ReceiveNetFieldExports: Invalid NetFieldExportHandle " + Export.Handle + ", Max " + Exports.length);
      }
    }
  }

  ReceiveNetExportGUIDs(Ar)
  {
//    check(Connection->InternalAck);
//    TGuardValue<bool> IsExportingGuard(GuidCache->IsExportingNetGUIDBunch, true);
//
//    uint32 NumGUIDs = 0;
//    Archive.SerializeIntPacked(NumGUIDs);
    let NumGUIDs = {val: new TypedVal('intPacked',0)};
    NumGUIDs = Ar.out(NumGUIDs,'val');
    //console.log('NumGUIDs: ' + NumGUIDs);

    for (let i = 0; i < NumGUIDs; i++)
    {
      //TArray<uint8> GUIDData;
      //Archive << GUIDData;
      let GUIDLength = {val: new TypedVal('int32',0)};
      GUIDLength = Ar.out(GUIDLength,'val');
      for (let j = 0; j < GUIDLength; j++){
        let GUID = {val: new TypedVal('uint8',0)};
        GUID = Ar.out(GUID,'val');
        //console.log('ExportGUID: ' + GUID); 
      }

      //FMemoryReader Reader(GUIDData);
      //UObject* Object = nullptr;
      //InternalLoadObject(Reader, Object, 0);
    }
  }
}
exports.FNetGUIDCacheObject = FNetGUIDCacheObject;
exports.FNetFieldExport = FNetFieldExport;
exports.FNetFieldExportGroup = FNetFieldExportGroup;
exports.UPackageMapClient = UPackageMapClient;
