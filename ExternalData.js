"use strict";
const {Decrypt} = require('./build/Release/FortniteReplayModule');
const {UDemoNetDriver} = require('./Engine/Classes/Engine/DemoNetDriver.js');
const {PlayerStateEncryptionKey} = require('./Game/Fortnite/PlayerStateEncryptionKey.js');
const {FArchive,TypedVal} = require("./Core/Public/Serialization/Archive.js");
const util = require('util');
const fs = require('fs');

if (process.argv[2] == undefined){
  console.log("Need a File");
  process.exit(0);
}

if (process.argv[3] == undefined){
  console.log("Need a key");
  process.exit(0);
}
let key = Number(process.argv[3]);

let DemoNetDriver = new UDemoNetDriver('.',process.argv[2]);
DemoNetDriver.InitConnection();
DemoNetDriver.ReadPlaybackDemoHeader();
let CheckpointCount = DemoNetDriver.ReplayStreamer.GetCheckpointCount();
let i = 0;
for (i = 0; i < CheckpointCount; i++){
  let CheckpointAr = DemoNetDriver.ReplayStreamer.GetCheckpointArchive(i);
  DemoNetDriver.LoadCheckpoint(CheckpointAr);
  //console.log(DemoNetDriver.DemoNetObject);
  //console.log(JSON.stringify(DemoNetDriver.PackageMapClient));
}
let ExternalData = DemoNetDriver.ExternalData;
let ext_str = Buffer([0x19,0xfb,0x01])
for (i = 0; i < ExternalData.length; i++){
  let data = ExternalData[i].Data;
  fs.writeFileSync('externaldata_' + i,data);
  let prefix = data.slice(0,3);
  if (prefix.equals(ext_str)){
    let Ar = new FArchive(data.slice(3,data.length));
    let Str = {val: new TypedVal('FString','')};
    Ar.out(Str,'val');
    Str = Str.val;
    //console.log(Str);
    let decomp_buf = Buffer.from(Str);
    fs.writeFileSync('externaldata_' + i + '_before',decomp_buf);
    Decrypt(decomp_buf,key);
    fs.writeFileSync('externaldata_' + i + '_decrypt',decomp_buf);
    //console.log(decomp_buf.toString('ascii'));
  }
}

//console.log(JSON.stringify(DemoNetDriver.ExternalData));
