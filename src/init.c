#include <node_api.h>

#include "module.h"
#include "FDateTime.h"
#include "Compression.h"
#include "Encryption.h"
#include "StringSearch.h"

napi_value Decrypt(napi_env env, napi_callback_info info){
  napi_status status;
  size_t argc = 2;
  napi_value argv[2], value;
  void *InOutBuffer;
  size_t InSize;
  uint32_t Key;

  status = napi_get_cb_info(env, info, &argc, argv, NULL, NULL);
  ON_ERROR(status,env,"Failed to parse arguments");

  status = napi_get_buffer_info(env, argv[0], &InOutBuffer,  &InSize);
  ON_ERROR(status,env,"Problem getting buffer");

  status = napi_get_value_uint32(env, argv[1], &Key);
  ON_ERROR(status,env,"Problem getting Key");

  if (decrypt(InOutBuffer,InSize,(uint8_t *)&Key) == 0)
     napi_throw_error(env,NULL,"Couldn't decrypt!!!");

  status = napi_create_uint32(env, 1, &value);
  ON_ERROR(status,env,"Can't create uint32");

  return value;
}

napi_value Decompress(napi_env env, napi_callback_info info){
  napi_status status;
  size_t argc = 2;
  napi_value argv[2], value;
  void *InBuffer, *OutBuffer;
  size_t InSize, OutSize;

  status = napi_get_cb_info(env, info, &argc, argv, NULL, NULL);
  ON_ERROR(status,env,"Failed to parse arguments");

  status = napi_get_buffer_info(env, argv[0], &InBuffer,  &InSize);
  ON_ERROR(status,env,"Problem getting buffer");

  status = napi_get_buffer_info(env, argv[1], &OutBuffer,  &OutSize);
  ON_ERROR(status,env,"Problem getting buffer");

  if (decompress(InBuffer,InSize,OutBuffer,OutSize) == 0)
     napi_throw_error(env,NULL,"Couldn't decompress!!!");

  status = napi_create_uint32(env, 1, &value);
  ON_ERROR(status,env,"Can't create uint32");

  return value;
}

napi_value StringSearchUTF8(napi_env env, napi_callback_info info){
  napi_status status;
  size_t argc = 2;
  napi_value argv[2], value;
  void *NeedleBuffer, *HaystackBuffer;
  size_t NeedleSize, HaystackSize, needle_pos;
  char *needle = NULL;

  status = napi_get_cb_info(env, info, &argc, argv, NULL, NULL);
  ON_ERROR(status,env,"Failed to parse arguments");

  status = napi_get_buffer_info(env, argv[0], &NeedleBuffer,  &NeedleSize);
  ON_ERROR(status,env,"Problem getting buffer");

  status = napi_get_buffer_info(env, argv[1], &HaystackBuffer,  &HaystackSize);
  ON_ERROR(status,env,"Problem getting buffer");

  needle_pos = simple_string_search((char *)NeedleBuffer,NeedleSize,(char *)HaystackBuffer,HaystackSize);

  if (needle_pos != HaystackSize)
  {
    size_t needle_length = 0;
    needle = (char *)HaystackBuffer + needle_pos;
    while(needle[needle_length] != '\0' &&  (needle_pos + needle_length) < HaystackSize){
      needle_length++;
    }
    status = napi_create_string_utf8(env, needle, needle_length, &value);
    ON_ERROR(status,env,"Couldn't create needle string");

    return value;
  }

  status = napi_get_null(env, &value);
  return value;
}

napi_value TicksToUnixTimestamp(napi_env env, napi_callback_info info){
  napi_status status;
  size_t argc = 1;
  napi_value argv[1], value;
  void *Buffer;
  size_t Size;
  int64_t Ticks;
  unsigned int UnixTime;

  status = napi_get_cb_info(env, info, &argc, argv, NULL, NULL);
  ON_ERROR(status,env,"Failed to parse arguments");

  status = napi_get_buffer_info(env, argv[0], &Buffer,  &Size);
  ON_ERROR(status,env,"Problem getting buffer");

  memcpy(&Ticks,Buffer, Size > 8 ? 8 : Size);
  UnixTime = ToUnixTimestamp(Ticks);

  status = napi_create_uint32(env, UnixTime, &value);
  ON_ERROR(status,env,"Can't create uint32");

  return value;

}

napi_value Init(napi_env env, napi_value exports) {
  napi_status status;
  napi_value fn;

  status = napi_create_function(env, NULL, 0, TicksToUnixTimestamp, NULL, &fn);
  ON_ERROR(status,env,"Unable to wrap native function");
  status = napi_set_named_property(env, exports, "TicksToUnixTimestamp", fn);
  ON_ERROR(status,env,"Unable to populate exports");

  status = napi_create_function(env, NULL, 0, Decompress, NULL, &fn);
  ON_ERROR(status,env,"Unable to wrap native function");
  status = napi_set_named_property(env, exports, "Decompress", fn);
  ON_ERROR(status,env,"Unable to populate exports");

  status = napi_create_function(env, NULL, 0, Decrypt, NULL, &fn);
  ON_ERROR(status,env,"Unable to wrap native function");
  status = napi_set_named_property(env, exports, "Decrypt", fn);
  ON_ERROR(status,env,"Unable to populate exports");

  status = napi_create_function(env, NULL, 0, StringSearchUTF8, NULL, &fn);
  ON_ERROR(status,env,"Unable to wrap native function");
  status = napi_set_named_property(env, exports, "StringSearchUTF8", fn);
  ON_ERROR(status,env,"Unable to populate exports");

  return exports;
}

NAPI_MODULE(NODE_GYP_MODULE_NAME, Init)
