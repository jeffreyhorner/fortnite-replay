#include "module.h"
#include <stdio.h>

#ifdef WIN32
#include <windows.h>
typedef int (__cdecl *oodle_decompress_t)(unsigned char *in, int insz, unsigned char *out, int outsz, int a, int b, int c, void *d, void *e, void *f, void *g, void *h, void *i, int j);
oodle_decompress_t oodle_decompress = NULL;

//Returns the last Win32 error, in string format. Returns an empty string if there is no error.
LPCTSTR GetLastErrorAsString()
{
  DWORD dLastError = GetLastError();
	LPCTSTR strErrorMessage = NULL;

	FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_ARGUMENT_ARRAY | FORMAT_MESSAGE_ALLOCATE_BUFFER,
		NULL,
		dLastError,
		0,
		(LPSTR) &strErrorMessage,
		0,
		NULL);

  return strErrorMessage;
}

void load_oodle(void){
  HINSTANCE lib = NULL;
  
  lib = LoadLibrary(TEXT("oo2core_5_win64.dll"));;
  if (lib==NULL){
    fprintf(stderr,"could not load oodle lib: %s\n",GetLastErrorAsString());
  }
  oodle_decompress = (oodle_decompress_t) GetProcAddress(lib, "OodleLZ_Decompress");
  if (oodle_decompress==NULL){
    fprintf(stderr,"could not get oodle_decompress: %s\n",GetLastErrorAsString());
  }
}
#endif

int decompress(unsigned char *in, int insz, unsigned char *out, int outsz){
#ifdef WIN32
  if (oodle_decompress == NULL) load_oodle();
    
  if (oodle_decompress == NULL) return 0;

  oodle_decompress(in,insz,out,outsz,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,3);
  return 1;
#endif
  return 1;
}
