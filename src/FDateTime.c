#include "FDateTime.h"

const int DaysPerMonth[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
const int DaysToMonth[] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };

/** The maximum number of ticks that can be represented in FTimespan. */
const int64_t MaxTicks = 9223372036854775807;

/** The minimum number of ticks that can be represented in FTimespan. */
const int64_t MinTicks = -9223372036854775807 - 1;

/** The number of nanoseconds per tick. */
const int64_t NanosecondsPerTick = 100;

/** The number of timespan ticks per day. */
const int64_t TicksPerDay = 864000000000;

/** The number of timespan ticks per hour. */
const int64_t TicksPerHour = 36000000000;

/** The number of timespan ticks per microsecond. */
const int64_t TicksPerMicrosecond = 10;

/** The number of timespan ticks per millisecond. */
const int64_t TicksPerMillisecond = 10000;

/** The number of timespan ticks per minute. */
const int64_t TicksPerMinute = 600000000;

/** The number of timespan ticks per second. */
const int64_t TicksPerSecond = 10000000;

/** The number of timespan ticks per week. */
const int64_t TicksPerWeek = 6048000000000;


double GetJulianDay(int64_t Ticks)
{
  return (double)(1721425.5 + Ticks / TicksPerDay);
}


bool IsLeapYear(int Year)
{
  if ((Year % 4) == 0)
  {
    return (((Year % 100) != 0) || ((Year % 400) == 0));
  }

  return false;
}

int64_t DateTime(int Year, int Month, int Day, int Hour, int Minute, int Second, int Millisecond)
{
  int TotalDays = 0;
  int64_t Ticks;

  if ((Month > 2) && IsLeapYear(Year))
  {
    ++TotalDays;
  }

  --Year;                     // the current year is not a full year yet
  --Month;                    // the current month is not a full month yet

  TotalDays += Year * 365;
  TotalDays += Year / 4;              // leap year day every four years...
  TotalDays -= Year / 100;            // ...except every 100 years...
  TotalDays += Year / 400;            // ...but also every 400 years
  TotalDays += DaysToMonth[Month];        // days in this year up to last month
  TotalDays += Day - 1;             // days in this month minus today

  Ticks = TotalDays * TicksPerDay
    + Hour * TicksPerHour
    + Minute * TicksPerMinute
    + Second * TicksPerSecond
    + Millisecond * TicksPerMillisecond;

  return Ticks;
}

int GetHour(int64_t Ticks)
{
  return (int)((Ticks / TicksPerHour) % 24);
}

int GetMinute(int64_t Ticks)
{
  return (int)((Ticks / TicksPerMinute) % 60);
}

void GetDate(int64_t Ticks, int *OutYear, int *OutMonth, int *OutDay) 
{
  // Based on FORTRAN code in:
  // Fliegel, H. F. and van Flandern, T. C.,
  // Communications of the ACM, Vol. 11, No. 10 (October 1968).

  int i, j, k, l, n;

  l = (int)floor(GetJulianDay(Ticks) + 0.5) + 68569;
  n = 4 * l / 146097;
  l = l - (146097 * n + 3) / 4;
  i = 4000 * (l + 1) / 1461001;
  l = l - 1461 * i / 4 + 31;
  j = 80 * l / 2447;
  k = l - 2447 * j / 80;
  l = j / 11;
  j = j + 2 - 12 * l;
  i = 100 * (n - 49) + i + l;

  *OutYear = i;
  *OutMonth = j;
  *OutDay = k;
}

void DateTimeToString(char *String,int64_t Ticks){
  int Year, Month, Day;

  GetDate(Ticks,&Year,&Month,&Day);
  snprintf(String,1024, "%d-%d-%d %d:%d",Year,Month,Day,GetHour(Ticks),GetMinute(Ticks));
}
int64_t FromUnixTimestamp8(int64_t UnixTime)
{
  return DateTime(1970, 1, 1,0,0,0,0) + UnixTime * TicksPerSecond;
}

int64_t FromUnixTimestamp4(int UnixTime)
{
  return DateTime(1970, 1, 1,0,0,0,0) + UnixTime * TicksPerSecond;
}

unsigned int ToUnixTimestamp(int64_t Ticks){
  return (unsigned int)( (Ticks - DateTime(1970, 1, 1,0,0,0,0)) / TicksPerSecond );
}

