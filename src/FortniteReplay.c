// FortniteReplayStats.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <stdbool.h>
#include <iconv.h>

#include "FortniteReplay.h"

const int DaysPerMonth[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
const int DaysToMonth[] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };

/** The maximum number of ticks that can be represented in FTimespan. */
const int64_t MaxTicks = 9223372036854775807;

/** The minimum number of ticks that can be represented in FTimespan. */
const int64_t MinTicks = -9223372036854775807 - 1;

/** The number of nanoseconds per tick. */
const int64_t NanosecondsPerTick = 100;

/** The number of timespan ticks per day. */
const int64_t TicksPerDay = 864000000000;

/** The number of timespan ticks per hour. */
const int64_t TicksPerHour = 36000000000;

/** The number of timespan ticks per microsecond. */
const int64_t TicksPerMicrosecond = 10;

/** The number of timespan ticks per millisecond. */
const int64_t TicksPerMillisecond = 10000;

/** The number of timespan ticks per minute. */
const int64_t TicksPerMinute = 600000000;

/** The number of timespan ticks per second. */
const int64_t TicksPerSecond = 10000000;

/** The number of timespan ticks per week. */
const int64_t TicksPerWeek = 6048000000000;


double GetJulianDay(int64_t Ticks)
{
  return (double)(1721425.5 + Ticks / TicksPerDay);
}


bool IsLeapYear(int Year)
{
  if ((Year % 4) == 0)
  {
    return (((Year % 100) != 0) || ((Year % 400) == 0));
  }

  return false;
}

int64_t DateTime(int Year, int Month, int Day, int Hour, int Minute, int Second, int Millisecond)
{
  int TotalDays = 0;
  int64_t Ticks;

  if ((Month > 2) && IsLeapYear(Year))
  {
    ++TotalDays;
  }

  --Year;                     // the current year is not a full year yet
  --Month;                    // the current month is not a full month yet

  TotalDays += Year * 365;
  TotalDays += Year / 4;              // leap year day every four years...
  TotalDays -= Year / 100;            // ...except every 100 years...
  TotalDays += Year / 400;            // ...but also every 400 years
  TotalDays += DaysToMonth[Month];        // days in this year up to last month
  TotalDays += Day - 1;             // days in this month minus today

  Ticks = TotalDays * TicksPerDay
    + Hour * TicksPerHour
    + Minute * TicksPerMinute
    + Second * TicksPerSecond
    + Millisecond * TicksPerMillisecond;

  return Ticks;
}

int GetHour(int64_t Ticks)
{
  return (int)((Ticks / TicksPerHour) % 24);
}

int GetMinute(int64_t Ticks)
{
  return (int)((Ticks / TicksPerMinute) % 60);
}

void GetDate(int64_t Ticks, int *OutYear, int *OutMonth, int *OutDay) 
{
  // Based on FORTRAN code in:
  // Fliegel, H. F. and van Flandern, T. C.,
  // Communications of the ACM, Vol. 11, No. 10 (October 1968).

  int i, j, k, l, n;

  l = (int)floor(GetJulianDay(Ticks) + 0.5) + 68569;
  n = 4 * l / 146097;
  l = l - (146097 * n + 3) / 4;
  i = 4000 * (l + 1) / 1461001;
  l = l - 1461 * i / 4 + 31;
  j = 80 * l / 2447;
  k = l - 2447 * j / 80;
  l = j / 11;
  j = j + 2 - 12 * l;
  i = 100 * (n - 49) + i + l;

  *OutYear = i;
  *OutMonth = j;
  *OutDay = k;
}

void DateTimeToString(char *String,int64_t Ticks){
  int Year, Month, Day;

  GetDate(Ticks,&Year,&Month,&Day);
  snprintf(String,1024, "%d-%d-%d %d:%d",Year,Month,Day,GetHour(Ticks),GetMinute(Ticks));
}
int64_t FromUnixTimestamp8(int64_t UnixTime)
{
  return DateTime(1970, 1, 1,0,0,0,0) + UnixTime * TicksPerSecond;
}

int64_t FromUnixTimestamp4(int UnixTime)
{
  return DateTime(1970, 1, 1,0,0,0,0) + UnixTime * TicksPerSecond;
}

int64_t ToUnixTimestamp(int64_t Ticks){
  return (Ticks - DateTime(1970, 1, 1,0,0,0,0)) / TicksPerSecond;
}

typedef struct ReplayBufferContext_
{
  void *Buffer;
  size_t Size;
  size_t Position;
  size_t ChunkPositionBegin;
  size_t ChunkSize;
  int ChunkType;
  ReplayFile *Replay;
} ReplayBufferContext;

size_t ReadContext(void *Dest, size_t Size, ReplayBufferContext *CTX);

UEString *WideCharToMultiChar(UEString *Str){
  if (Str->UCS2==0) return Str;

 // for (unsigned int i = 0; i < Str->Size-1; i+=2){
  //  if ((unsigned char)Str->Buffer[i] == 0xff && (unsigned char)Str->Buffer[i+1] == 0xff){
   //   Str->Buffer[i] = '\0'; // Terminat since 0xffff isn't a real unicode character.
    //}
//  }

  iconv_t conver_d = iconv_open("UTF8","UCS2");

  if (conver_d == (iconv_t) -1 ){
    fprintf(stderr,"cannot open iconv descriptor\n");
    return Str;
  }

  char *MultiChar = malloc(Str->Size*2);
  size_t MultiCharSize = Str->Size*2;
  char *inbuf = Str->Buffer;
  char *outbuf = MultiChar;
  size_t inbytes = Str->Size;
  size_t outbytes = Str->Size*2;
  size_t status;

  fprintf(stderr,"Str->Size: %lu\n",Str->Size);
  fprintf(stderr,"inbytes: %lu\n",inbytes);
  fprintf(stderr,"outbytes: %lu\n",outbytes);
  status = iconv(conver_d,&inbuf, &inbytes, &outbuf, &outbytes);
  fprintf(stderr,"iconv: converted %lu (inbytes=%lu) ucs2 chars to %lu (outbytes=%lu) utf8 chars\n",(Str->Size/2) - inbytes,inbytes, MultiCharSize - outbytes,outbytes);

  if (status == (size_t) -1){
    if (errno == EINVAL) {
      perror("iconv EINVAL error");
    } else {
      perror("fatal iconv error");
    }
  }

  //MultiChar[MultiCharSize-outbytes-1] = '\0';
  *outbuf = '\0';
  free(Str->Buffer);
  Str->Buffer = MultiChar;
  Str->Size = MultiCharSize-outbytes;
  Str->UCS2 = 0;
  fprintf(stderr,"MultiChar(%lu): %s\n",Str->Size,Str->Buffer);
  return Str;
}

// Assumes StrSize includes null byte
UEString *GetUEString(ReplayBufferContext *CTX){
  UEString *NewStr = malloc(sizeof(UEString));
  int StrSize = 0, UCS2Check = 0;

  // 4 byte length 
  ReadContext(&StrSize,sizeof(int),CTX);

  // Empty string
  if (StrSize == 0 || StrSize == 1){
    NewStr->Buffer = malloc(1);
    NewStr->Buffer[0] = '\0';
    NewStr->UCS2 = 0;
    NewStr->Size = 1;
    return NewStr;
  }

  // UCS2 check
  if (StrSize < 0) {
    UCS2Check = 1;
    StrSize = -StrSize;
  }

  // Still less than 0, then corrupt
  if (StrSize < 0){
    perror("corrupt fstring!!!\n");
    return NULL;
  }

  // Presumption
  if (StrSize > 1024){
    perror("String size too long !!!\n");
    return NULL;
  }

  if (UCS2Check==1) {
    StrSize *= 2;
  }

  NewStr->Buffer = malloc(StrSize);
  if (UCS2Check==1)
    ReadContext(NewStr->Buffer,StrSize,CTX);
  else
    ReadContext(NewStr->Buffer,StrSize,CTX);
  NewStr->Buffer[StrSize-1] = '\0';
  NewStr->Size = StrSize;
  NewStr->UCS2 = UCS2Check;

  return WideCharToMultiChar(NewStr);
}

#define FORT_CHANGELIST_4_1 0x3dda1c
#define FORT_CHANGELIST_4_2 0x3e233a
#define IS_FORT_4_1(X) ((X)==FORT_CHANGELIST_4_1)
#define IS_FORT_4_2(X) ((X)==FORT_CHANGELIST_4_2)

size_t SeekContext(size_t Seek, ReplayBufferContext *CTX){
  if ( (CTX->Position + Seek) >= CTX->Size) return 0;

  CTX->Position += Seek;

  return Seek;
}

int AtEndOfContext(ReplayBufferContext *CTX){
  
  if (CTX->Position >= CTX->Size) {
    return 1;
  }
  return 0;
}

size_t ReadContext(void *Dest, size_t Size, ReplayBufferContext *CTX){
  if ( (CTX->Position + Size) > CTX->Size ) return 0;

  memcpy(Dest,CTX->Buffer + CTX->Position, Size);
  CTX->Position += Size;

  return Size;
}

void FreeUEString(UEString *String){
  free(String->Buffer);
  free(String);
}

void FreeEvent(ReplayEvent *Event);
void FreeReplayHeader(ReplayHeader *Header){
  if (Header->FriendlyName) FreeUEString(Header->FriendlyName);
  if (Header->Release) FreeUEString(Header->Release);
  if (Header->Map) FreeUEString(Header->Map);
}
void FreeReplayFile(ReplayFile *Replay){
  int i;

  if (Replay == NULL) return;

  ReplayBufferContext *CTX = (ReplayBufferContext *)((char *)Replay - sizeof(ReplayBufferContext));

  if (Replay->Events){
    for(i = 0; i < (int)Replay->NumEvents; i++) FreeEvent(Replay->Events[i]);
    free(Replay->Events);
  }

  if (Replay->Header) FreeReplayHeader(Replay->Header);
  free(CTX);
}

void ParsePlayerElimEvent(ReplayEvent *Event, ReplayBufferContext *CTX){

  Event->Event.PlayerElim = malloc(sizeof(PlayerElimEvent));

  if (IS_FORT_4_1(CTX->Replay->Header->ChangeList))
  {
    SeekContext(12,CTX);
  }
  else if (IS_FORT_4_2(CTX->Replay->Header->ChangeList))
  {
    SeekContext(40,CTX);
  } else {
    fprintf(stderr,"Unknown ChangeList: %d\n",CTX->Replay->Header->ChangeList);
    exit(0);
  }

  Event->Event.PlayerElim->Eliminated = GetUEString(CTX);
  Event->Event.PlayerElim->Eliminator = GetUEString(CTX);

  ReadContext(&(Event->Event.PlayerElim->ElimType),sizeof(int),CTX);

}

void FreePlayerElimEvent(PlayerElimEvent *Event){
  FreeUEString(Event->Eliminated);
  FreeUEString(Event->Eliminator);
  free(Event);
}

void ParseReplayBrowserEvent(ReplayEvent *Event, ReplayBufferContext *CTX){


  if (strcmp(UEStringBuffer(Event->MetaData),"AthenaMatchTeamStats")==0)
  {
    Event->Event.TeamStats = malloc(sizeof(TeamStatsEvent));
    SeekContext(4,CTX);
    ReadContext(&(Event->Event.TeamStats->Placement),sizeof(int),CTX);
    ReadContext(&(Event->Event.TeamStats->NumPlayers),sizeof(int),CTX);

  } else if (strcmp(UEStringBuffer(Event->MetaData),"AthenaMatchStats")==0)
  {
    Event->Event.MatchStats = malloc(sizeof(MatchStatsEvent));
    SeekContext(12,CTX);
    ReadContext(&(Event->Event.MatchStats->TotalEliminations),sizeof(int),CTX);
  }
}

void FreeReplayBrowserEvent(ReplayEvent *Event){
  if (strcmp(UEStringBuffer(Event->MetaData),"AthenaMatchTeamStats")==0)
  {
    free(Event->Event.TeamStats);

  } else if (strcmp(UEStringBuffer(Event->MetaData),"AthenaMatchStats")==0)
  {
    free(Event->Event.MatchStats);
  }
}

void ParseChunkEvent(ReplayBufferContext *CTX){
  ReplayEvent *Event = malloc(sizeof(ReplayEvent));
  ReplayFile *Replay = CTX->Replay;

  Replay->NumEvents++;
  if (Replay->Events!=NULL)
    Replay->Events = realloc(Replay->Events,sizeof(ReplayEvent *) * Replay->NumEvents);
  else
    Replay->Events = malloc(sizeof(ReplayEvent *) * Replay->NumEvents);

  Replay->Events[Replay->NumEvents-1] = Event;

  Event->Id = GetUEString(CTX);
  Event->Group = GetUEString(CTX);
  Event->MetaData = GetUEString(CTX);
  ReadContext(&(Event->Time1),sizeof(unsigned int),CTX);
  ReadContext(&(Event->Time2),sizeof(unsigned int),CTX);

  SeekContext(4,CTX); // size of event

  if (strcmp(UEStringBuffer(Event->Group),"playerElim") == 0)
  {
    ParsePlayerElimEvent(Event, CTX);
  }
  else if (strcmp(UEStringBuffer(Event->Group),"AthenaReplayBrowserEvents")==0)
  {
    ParseReplayBrowserEvent(Event, CTX);
  }
}

void FreeEvent(ReplayEvent *Event){

  if (strcmp(UEStringBuffer(Event->Group),"playerElim") == 0)
  {
    FreePlayerElimEvent(Event->Event.PlayerElim);
  }
  else if (strcmp(UEStringBuffer(Event->Group),"AthenaReplayBrowserEvents")==0)
  {
    FreeReplayBrowserEvent(Event);
  }

  FreeUEString(Event->Id);
  FreeUEString(Event->Group);
  FreeUEString(Event->MetaData);
  free(Event);
  
}


ReplayFile *ParseReplayFileBuffer(void *FileContent, size_t FileSize, char **ReplayError)
{
  unsigned int magic=0;
  ReplayHeader *Header=NULL;
  ReplayFile *Replay=NULL;
  ReplayBufferContext *CTX = 
    calloc(1,
      sizeof(ReplayBufferContext) +
      sizeof(ReplayFile) + 
      sizeof(ReplayHeader)
    );

  if (CTX==NULL) return NULL;

  CTX->Buffer = FileContent;
  CTX->Size = FileSize;
  CTX->Position = 0;

  Replay = CTX->Replay =  (ReplayFile *)((char *)CTX + sizeof(ReplayBufferContext));
  Replay->NumEvents = 0;
  Replay->Events = NULL;

  Header = Replay->Header = (ReplayHeader *)((char *)Replay + sizeof(ReplayFile));
  
  // Magic Number
  ReadContext(&magic,sizeof(unsigned int),CTX);
  if (magic != 0x1CA2E27F){
    *ReplayError = strdup("Bad Magic Number. Not a replay file.");
    FreeReplayFile(CTX->Replay);
    return NULL;
  }

  // Version
  ReadContext(&(Header->FileVersion),sizeof(unsigned int),CTX);

  // Length In Milliseconds
  ReadContext(&(Header->LengthInMS),sizeof(int),CTX);

  // Network Version
  ReadContext(&(Header->NetworkVersion),sizeof(unsigned int),CTX);

  // Changelist
  ReadContext(&(Header->ChangeList),sizeof(unsigned int),CTX);

  // FriendlyName
  Header->FriendlyName = GetUEString(CTX);

  // IsLive: will be 1 when in game, 0 when game over
  ReadContext(&(Header->IsLive),sizeof(int),CTX);

  // Date/Time of Match
  ReadContext(&(Header->UnrealTicks),sizeof(int64_t),CTX);
  //DateTimeToString(cbuf,Ticks);

  // Unknown 42 bytes
  SeekContext(42,CTX);

  // Release String
  Header->Release = GetUEString(CTX);

  // Unknown 4 bytes
  SeekContext(4,CTX);

  // Map
  Header->Map = GetUEString(CTX);

  // Unknown bytes
  SeekContext(12,CTX);

  // Chunktypes:
  // 0 - Header
  // 1 - Replay Data
  // 2 - Checkpoint
  // 3 - Event
  do {
    // CHunkType
    if (ReadContext(&(CTX->ChunkType),sizeof(int),CTX)==0){
      if (AtEndOfContext(CTX)) return Replay;
      *ReplayError = strdup("Replay file corrupt. Couldn't get next chunk size");
      FreeReplayFile(Replay);
      return NULL;
    }
   
    // SizeInBytes
    ReadContext(&(CTX->ChunkSize),sizeof(int),CTX);

    CTX->ChunkPositionBegin = CTX->Position;

    // Chunk
    if (CTX->ChunkType==3){
      //fprintf(stderr,"(Event)\n");
      //parse_chunk_event(rp,i,ftell(rp));
      ParseChunkEvent(CTX);
    }

    CTX->Position = CTX->ChunkPositionBegin + CTX->ChunkSize;
  } while(!AtEndOfContext(CTX));

  return Replay;
}
