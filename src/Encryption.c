#include "module.h"
#include "Encryption.h"
#include <stdio.h>

// XOR
void shuffle_key(uint8_t *Key){
  unsigned int shuffle = *( (unsigned int*)Key );
  uint8_t *shuffle_key = Key;

  for (int i = 0, j = 4; i < 4; i++, j--){
    shuffle_key[i] = ((uint8_t *)&shuffle)[j];
  }
}
int decrypt(uint8_t *Block, unsigned int BytesCount, uint8_t *Key){
  //shuffle_key(Key);
  for (unsigned int i = 0; i < BytesCount; ++i)
  {
    Block[i] ^= Key[i % 3];
  }
  return 1;
}
