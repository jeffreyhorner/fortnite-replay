#include <stdio.h>
#include <stdlib.h>
// Find position of needle in haystack
size_t simple_string_search(char *needle, size_t needle_length, char *haystack, size_t haystack_length){
  size_t match_length = 0, haystack_pos = 0, found_needle_pos = 0;
  char *match_needle = NULL;


  if (haystack_length <= 0 || needle_length<= 0 || needle==NULL || haystack==NULL) return haystack_length;

  while(haystack_pos < haystack_length){
    match_needle = needle;
    match_length = needle_length;
    found_needle_pos = haystack_pos;
    while(match_length && haystack_pos < haystack_length && *match_needle == *haystack){
      match_length--;
      match_needle++;
      haystack++;
      haystack_pos++;
    }
    if (match_length == 0){
      return found_needle_pos;
    } else {
      if (match_length <= 3){
        fprintf(stderr,"match_length: %u\n",match_length);
      }
    }

    haystack++;
    haystack_pos++;
  }
  
  return haystack_length;
}
