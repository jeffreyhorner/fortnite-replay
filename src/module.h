#define ON_ERROR(status,env,error) \
  if ((status) != napi_ok) napi_throw_error((env),NULL,(error))

#define SET_SIMPLE_PROP(obj,prop_name,prop_val,type,val_var,env,status) \
  status = napi_create_ ## type( (env), (prop_val), &(val_var) ); \
  if ( (status) != napi_ok) napi_throw_error((env),NULL,"Couldn't create " #prop_name ":" #type ); \
  status = napi_set_named_property( (env), (obj), (prop_name), (val_var) ); \
  if ( (status) != napi_ok) napi_throw_error((env),NULL,"Couldn't set property " #prop_name ":" #type)
