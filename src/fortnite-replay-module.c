#include <node_api.h>
#include "FortniteReplay.h"
#include <string.h>
#include <stdlib.h>

#define ON_ERROR(status,env,error) \
  if ((status) != napi_ok) napi_throw_error((env),NULL,(error))

#define SET_SIMPLE_PROP(obj,prop_name,prop_val,type,val_var,env,status) \
  status = napi_create_ ## type( (env), (prop_val), &(val_var) ); \
  if ( (status) != napi_ok) napi_throw_error((env),NULL,"Couldn't create " #prop_name ":" #type ); \
  status = napi_set_named_property( (env), (obj), (prop_name), (val_var) ); \
  if ( (status) != napi_ok) napi_throw_error((env),NULL,"Couldn't set property " #prop_name ":" #type)
  
#define SET_UTF8STRING_PROP(obj,prop_name,prop_val,val_var,env,status) \
  status = napi_create_string_utf8( \
      (env), \
      UEStringBuffer( (prop_val) ), \
      UEStringSize( (prop_val) ) - 1, \
      &(val_var) \
    ); \
  if ( (status) != napi_ok) napi_throw_error((env),NULL,"Couldn't create utf8 string " #prop_name); \
  status = napi_set_named_property( (env), (obj), (prop_name), (val_var) ); \
  if ( (status) != napi_ok) napi_throw_error((env),NULL,"Couldn't set property " #prop_name " for utf8 string")

#define SET_UTF16STRING_PROP(obj,prop_name,prop_val,val_var,env,status) \
  status = napi_create_string_utf16( \
      (env), \
      (char16_t *)UEStringBuffer( (prop_val) ), \
      (UEStringSize( (prop_val) ) - 1) / 2, \
      &(val_var) \
    ); \
  if ( (status) != napi_ok) napi_throw_error((env),NULL,"Couldn't create utf16 string " #prop_name); \
  status = napi_set_named_property( (env), (obj), (prop_name), (val_var) ); \
  if ( (status) != napi_ok) napi_throw_error((env),NULL,"Couldn't set property " #prop_name " for utf16 string")

#define SET_UESTRING_PROP(obj,prop_name,prop_val,type,val_var,env,status) \
  status = napi_create_string_ ## type( \
      (env), \
      UEStringBuffer( (prop_val) ), \
      UEStringSize( (prop_val) ) - 1, \
      &(val_var) \
    ); \
  if ( (status) != napi_ok) napi_throw_error((env),NULL,"Couldn't create string " #prop_name ":" #type ); \
  status = napi_set_named_property( (env), (obj), (prop_name), (val_var) ); \
  if ( (status) != napi_ok) napi_throw_error((env),NULL,"Couldn't set property " #prop_name ":" #type )

napi_value ParseReplay(napi_env env, napi_callback_info info) {
  napi_status status;
  size_t argc = 1;
  napi_value argv[1], replay, value, events, event;
  void *FileBuffer;
  size_t FileSize;
  ReplayFile *Replay;
  char *ReplayError;

  status = napi_get_cb_info(env, info, &argc, argv, NULL, NULL);
  ON_ERROR(status,env,"Failed to parse arguments");

  status = napi_get_buffer_info(env, argv[0], &FileBuffer,  &FileSize);
  ON_ERROR(status,env,"Invalid number was passed as argument");

  Replay = ParseReplayFileBuffer(FileBuffer, FileSize, &ReplayError);

  if (Replay == NULL){
    status = napi_throw_error(env,NULL,ReplayError);
    free(ReplayError);
    status = napi_get_null(env, &replay);
    return replay;
  }

  status = napi_create_object(env, &replay);
  ON_ERROR(status,env,"Couldn't create header object");

  SET_SIMPLE_PROP(replay,"FileVersion",Replay->Header->FileVersion,uint32,value,env,status);
  SET_SIMPLE_PROP(replay,"LengthInMS",Replay->Header->LengthInMS,int32,value,env,status);
  SET_UESTRING_PROP(replay,"Release",Replay->Header->Release,utf8,value,env,status);
  SET_SIMPLE_PROP(replay,"ChangeList",Replay->Header->ChangeList,uint32,value,env,status);
  SET_UESTRING_PROP(replay,"FriendlyName",Replay->Header->FriendlyName,utf8,value,env,status);
  SET_SIMPLE_PROP(replay,"IsLive",Replay->Header->IsLive,int32,value,env,status);
  SET_SIMPLE_PROP(replay,"DateTimeInTicks",Replay->Header->UnrealTicks,int64,value,env,status);
  SET_SIMPLE_PROP(replay,"DateTimeUnixTimestamp",ToUnixTimestamp(Replay->Header->UnrealTicks),int64,value,env,status);
  SET_UESTRING_PROP(replay,"Map",Replay->Header->Map,utf8,value,env,status);

  status = napi_create_array(env, &events);
  ON_ERROR(status,env,"Couldn't create array object");
  status = napi_set_named_property(env, replay, "Events", events);

  for (unsigned int i = 0; i < Replay->NumEvents; i++){
    ReplayEvent *Event = Replay->Events[i];

    status = napi_create_object(env, &event);
    ON_ERROR(status,env,"Couldn't create event object");

    SET_SIMPLE_PROP(event,"Time1",Event->Time1,uint32,value,env,status);
    SET_SIMPLE_PROP(event,"Time2",Event->Time2,uint32,value,env,status);
    SET_UESTRING_PROP(event,"Id",Event->Id,utf8,value,env,status);
    SET_UESTRING_PROP(event,"Group",Event->Group,utf8,value,env,status);
    SET_UESTRING_PROP(event,"MetaData",Event->MetaData,utf8,value,env,status);

    status = napi_set_element(env,events,i, event);

    if (strcmp(UEStringBuffer(Event->Group),"playerElim") == 0)
    {
      PlayerElimEvent *e = Event->Event.PlayerElim;

      if (UEStringUCS2(e->Eliminator))
        SET_UTF16STRING_PROP(event,"Eliminator",e->Eliminator,value,env,status);
      else
        SET_UTF8STRING_PROP(event,"Eliminator",e->Eliminator,value,env,status);

      if (UEStringUCS2(e->Eliminated))
        SET_UTF16STRING_PROP(event,"Eliminated",e->Eliminated,value,env,status);
      else
        SET_UTF8STRING_PROP(event,"Eliminated",e->Eliminated,value,env,status);
    }
    else if (strcmp(UEStringBuffer(Event->Group),"AthenaReplayBrowserEvents")==0)
    {
      if (strcmp(UEStringBuffer(Event->MetaData),"AthenaMatchTeamStats")==0)
      {
        TeamStatsEvent *e = Event->Event.TeamStats;
        SET_SIMPLE_PROP(event,"Placement",e->Placement,int32,value,env,status);
        SET_SIMPLE_PROP(event,"NumPlayers",e->NumPlayers,int32,value,env,status);

      } else if (strcmp(UEStringBuffer(Event->MetaData),"AthenaMatchStats")==0)
      {
        MatchStatsEvent *e = Event->Event.MatchStats;
        SET_SIMPLE_PROP(event,"TotalElims",e->TotalEliminations,int32,value,env,status);
      }
    }
  }

  return replay;
}

napi_value Init(napi_env env, napi_value exports) {
  napi_status status;
  napi_value fn;

  status = napi_create_function(env, NULL, 0, ParseReplay, NULL, &fn);
  ON_ERROR(status,env,"Unable to wrap native function");
  status = napi_set_named_property(env, exports, "ParseReplay", fn);
  ON_ERROR(status,env,"Unable to populate exports");

  return exports;
}

NAPI_MODULE(NODE_GYP_MODULE_NAME, Init)
