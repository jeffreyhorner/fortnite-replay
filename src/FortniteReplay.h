typedef struct UEString_
{
  char *Buffer;
  size_t Size;
  int UCS2;
} UEString;

static inline char *UEStringBuffer(UEString  *String){
  return String->Buffer;
}

static inline size_t UEStringSize(UEString *String){
  return String->Size;
}

static inline int UEStringUCS2(UEString *String){
  return String->UCS2;
}

typedef struct ReplayHeader_
{
  int64_t UnrealTicks;
  int64_t UnixDateTimeOfMatch;
  unsigned int FileVersion;
  int LengthInMS;
  unsigned int NetworkVersion;
  unsigned int ChangeList;
  int IsLive;
  UEString *ISO8601DateTimeOfMatch;
  UEString *FriendlyName;
  UEString *Release; 
  UEString *Map;
} ReplayHeader;

typedef struct PlayerElimEvent_
{
  UEString *Eliminator;
  UEString *Eliminated;
  int ElimType;
} PlayerElimEvent;

typedef struct TeamStatsEvent_
{
  int Placement;
  int NumPlayers;
} TeamStatsEvent;

typedef struct MatchStatsEvent_
{
  int TotalEliminations;
} MatchStatsEvent; 

typedef struct ReplayEvent_
{
  unsigned int Time1;
  unsigned int Time2;
  UEString *Id;
  UEString *Group;
  UEString *MetaData;
  union {
    PlayerElimEvent *PlayerElim;
    TeamStatsEvent *TeamStats;
    MatchStatsEvent *MatchStats;
  } Event;
} ReplayEvent;

typedef struct ReplayFile_
{
  ReplayHeader *Header;
  unsigned int NumEvents;
  ReplayEvent **Events;
} ReplayFile;


ReplayFile *ParseReplayFileBuffer(void *FileContent, size_t FileSize, char **ReplayError);
void FreeReplayFile(ReplayFile *Replay);
int64_t ToUnixTimestamp(int64_t Ticks);
