size_t simple_string_search(char *needle, size_t needle_length, char *haystack, size_t haystack_length);
