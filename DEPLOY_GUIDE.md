# Configure AWS Windows server

1. Remote into the computer with RDP
2. Start windows powershell as Administrator

3. Enable PS Remoting

```
Enable-PSRemoting –force
```

4. Configure PS Remoting

```
winrm quickconfig # type yes at the prompt
```

5. Turn off firewall

```
netsh advfirewall set allprofiles state off
```

# Configure your Windows client

1. Configure local PS

```
winrm quickconfig # type yes at the prompts
```

2. Add remote IP to trusted IP list

```
Set-Item WSMan:\localhost\Client\TrustedHosts $ip
```

# Build Deploy of fortnite-replay from your Windows client

1. Download repos

```
git clone git@gitlab.com:jeffreyhorner/fortnite-replay.git
```

2. Enter local repos and run build-deploy.ps1

```
cd .\fortnite-replay
. .\build-deploy.ps1
```

3. Deploy to Ec2 Instance List

```
$i = Load-Ec2InstanceList "fortnite-replay-windows"
Foreach($name in $i){
  $s = New-EC2PSSession $name
  Copy-Item ..\fortnite-replay-deploy.zip -Destination C:\Users\Administrator\Documents\. -ToSession $s
  Invoke-Command -Session $s -FilePath .\remote-install.ps1 
}
```
