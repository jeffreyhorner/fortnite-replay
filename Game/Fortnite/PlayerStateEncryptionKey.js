const Fortnite = require('../../Game/Fortnite/Fortnite.js');
const {TypedVal, FArchive} = require('../../Core/Public/Serialization/Archive.js');

class PlayerStateEncryptionKey
{
  constructor(){
    this.Key = new TypedVal('uint32',0);
  }

  out(Ar,Info,Event){
    console.log(Event);
    Ar.Seek(Event.EventDataOffset);
    Ar.out(this,'Key');
  }
}
exports.PlayerStateEncryptionKey = PlayerStateEncryptionKey;
