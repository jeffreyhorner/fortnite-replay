const {StringSearchUTF8} = require('../../build/Release/FortniteReplayModule');

exports.FindPlaylist = function(ChunkBuffer){
  let playlistBuffer = Buffer.from("/Game/Athena/Playlists/");
  let foundPlaylistString =  StringSearchUTF8(playlistBuffer,ChunkBuffer);
  if (foundPlaylistString != null){
    return foundPlaylistString.split('/').reverse()[0].split('Playlist_')[1];
  }
  return null;
}
