const Fortnite = require('../../Game/Fortnite/Fortnite.js');
const {TypedVal, FArchive} = require('../../Core/Public/Serialization/Archive.js');

class AthenaMatchTeamStats
{
  constructor(){
    this.Placement = new TypedVal('int32','');
    this.NumPlayers = new TypedVal('int32','');
  }

  out(Ar,Info,Event){
    let offset = 0;

    Ar.Seek(Event.EventDataOffset + 4)
    Ar.out(this,'Placement');
    Ar.out(this,'NumPlayers');
  }
}
exports.AthenaMatchTeamStats = AthenaMatchTeamStats;
