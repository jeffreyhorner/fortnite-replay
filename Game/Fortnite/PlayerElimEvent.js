const Fortnite = require('../../Game/Fortnite/Fortnite.js');
const {TypedVal, FArchive} = require('../../Core/Public/Serialization/Archive.js');

class PlayerElimEvent
{
  constructor(){
    this.Eliminator = new TypedVal('FString','');
    this.Eliminated = new TypedVal('FString','');
    this.ElimType = new TypedVal('int32',0);
  }

  out(Ar,Info,Event){
    let offset = 0;

    if (Fortnite.IsFort41(Info.Changelist))
      offset = 12;
    else if (Fortnite.IsFort42(Info.Changelist))
      offset = 40;
    else if (Fortnite.IsFort43(Info.Changelist))
      offset = 45;
    else if (Fortnite.IsFort431(Info.Changelist))
      offset = 45;
    else if (Fortnite.IsFort44(Info.Changelist))
      offset = 45;
    else
      offset = 45;
      

    Ar.Seek(Event.EventDataOffset + offset);
    Ar.out(this,'Eliminated');
    Ar.out(this,'Eliminator');
    Ar.out(this,'ElimType');
  }
}
exports.PlayerElimEvent = PlayerElimEvent;
