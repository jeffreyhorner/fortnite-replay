"use strict";
const {UDemoNetDriver} = require('../../Engine/Classes/Engine/DemoNetDriver.js');
const {FindPlaylist}  = require('./Playlist.js');
const {PlayerElimEvent} = require('./PlayerElimEvent.js');
const {AthenaMatchStats} = require('./AthenaMatchStats.js');
const {AthenaMatchTeamStats} = require('./AthenaMatchTeamStats.js');
const fs = require('fs');

const FORT_CHANGELIST_4_1 = 0x3dda1c;
const FORT_CHANGELIST_4_2 = 0x3e233a;
const FORT_CHANGELIST_4_3 = 0x3e7f3e;
const FORT_CHANGELIST_4_3_1 =  0x3e9c90;
const FORT_CHANGELIST_4_4 = 4117433;

exports.IsFort41 = function(Changelist){
  return Changelist == FORT_CHANGELIST_4_1;
}

exports.IsFort42 = function(Changelist){
  return Changelist == FORT_CHANGELIST_4_2;
}

exports.IsFort43 = function(Changelist){
  return Changelist == FORT_CHANGELIST_4_3;
}

exports.IsFort431 = function(Changelist){
  return Changelist == FORT_CHANGELIST_4_3_1;
}
exports.IsFort44 = function(Changelist){
  return Changelist == FORT_CHANGELIST_4_4;
}

exports.BasicReplayStatsFromBuffer = function(ReplayBuffer) {
  let DemoNetDriver = new UDemoNetDriver();
  DemoNetDriver.InitConnection(ReplayBuffer);
  DemoNetDriver.ReadPlaybackDemoHeader();

  // Basic Info
  let BasicInfo = DemoNetDriver.ReplayStreamer.GetBasicInfo();
  let dt = new Date(BasicInfo.Timestamp * 1000).toISOString();
  BasicInfo.DateTime = dt.split('.')[0].split('T').join(' ');

  // GameMode
  let info = DemoNetDriver.ReplayStreamer.GetInfo();
  let datachunks = info.DataChunks;
  let playlist = null;
  for (let i = 0; i < datachunks.length; i++){
    playlist = FindPlaylist(datachunks[i].ReplayBuffer);
    if (playlist != null) break;
  }
  BasicInfo.GameMode = playlist;

  // MatchStats and PlayerElims
  let replayAr = DemoNetDriver.ReplayStreamer.ReplayAr;
  let events = info.Events;
  let matchStats = null;
  let matchTeamStats = null;
  let playerElim = null;
  let playerElims = [];
  for (let i = 0; i < events.length; i++){
    if (events[i].Metadata=='AthenaMatchStats'){
      matchStats = new AthenaMatchStats();
      matchStats.out(replayAr,info,events[i]);
    } else if (events[i].Metadata=='AthenaMatchTeamStats'){
      matchTeamStats = new AthenaMatchTeamStats();
      matchTeamStats.out(replayAr,info,events[i]);
    } else if (events[i].Group=='playerElim'){
      playerElim = new PlayerElimEvent();
      playerElim.out(replayAr,info,events[i]);
      playerElim.Time = events[i].Time1;
      playerElims.push(playerElim);
    }
  }
  BasicInfo = Object.assign(BasicInfo,matchStats,matchTeamStats);
  BasicInfo.PlayerEliminations = playerElims;

  // Version info
  let engine = DemoNetDriver.DemoHeader.EngineVersion
  BasicInfo.Version = engine.Major + '.' + engine.Minor + '.' + engine.Patch + engine.Branch;
  return(BasicInfo);
}

exports.BasicReplayStatsFromFile = function(ReplayFile){
  let buf = fs.readFileSync(ReplayFile);
  return (exports.BasicReplayStatsFromBuffer(buf));
}
