const Fortnite = require('../../Game/Fortnite/Fortnite.js');
const {TypedVal, FArchive} = require('../../Core/Public/Serialization/Archive.js');

class AthenaMatchStats
{
  constructor(){
    this.TotalElims = new TypedVal('int32','');
  }

  out(Ar,Info,Event){
    let offset = 0;

    Ar.Seek(Event.EventDataOffset + 12)
    Ar.out(this,'TotalElims');
  }
}
exports.AthenaMatchStats = AthenaMatchStats;
