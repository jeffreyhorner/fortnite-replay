"use strict";

Array.prototype.IsValidIndex = function(i){
  return (this.length > 0 && i >= 0 && i < this.length);
}

const EventEmitter = require('events');
const fs = require('fs');
const int64 = require('int64_t');
const {TypedVal, FArchive} = require('../../../Core/Public/Serialization/Archive.js');
const {FNetworkDemoHeader} = require('../../../Engine/Classes/Engine/DemoNetDriver.js');
const {Decompress} = require('../../../build/Release/FortniteReplayModule');

const INDEX_NONE = 0xFFFFFFFF;
const FileMagic = 0x1CA2E27F;

const ELocalFileChunkType =
{
  Header: 0,
  ReplayData: 1,
  Checkpoint: 2,
  Event: 3,
  Unknown: INDEX_NONE
};

const ELocalFileVersionHistory =
{
  HISTORY_INITIAL: 0,
  HISTORY_FIXEDSIZE_FRIENDLY_NAME: 1,
  HISTORY_COMPRESSION: 2,
  HISTORY_RECORDED_TIMESTAMP: 3,
  HISTORY_STREAM_CHUNK_TIMES: 4,

  // -----<new versions can be added before this line>-------------------------------------------------
  HISTORY_PLUS_ONE: INDEX_NONE - 1,
  HISTORY_LATEST: INDEX_NONE
};


class FLocalFileChunkInfo
{
  //ELocalFileChunkType ChunkType;
  //int32 SizeInBytes;
  //int64 TypeOffset;
  //int64 DataOffset;

  constructor(){
    this.ChunkType = new TypedVal('uint32',INDEX_NONE);
    this.SizeInBytes = new TypedVal('int32',0);
    this.TypeOffset = new TypedVal('int64',0);
    this.DataOffset = new TypedVal('int64',0);
  }
}
class FLocalFileReplayDataInfo
{
  //int32 ChunkIndex;
  //uint32 Time1;
  //uint32 Time2;
  //int32 SizeInBytes;
  //int64 ReplayDataOffset;
  //int64 StreamOffset;
  
  constructor(){
    this.ChunkIndex = new TypedVal('uint32',INDEX_NONE);
    this.Time1 = new TypedVal('uint32',0);
    this.Time2 = new TypedVal('uint32',0);
    this.SizeInBytes = new TypedVal('int32',0);
    this.ReplayDataOffset = new TypedVal('int64',0);
    this.StreamOffset = new TypedVal('int64',0);
    this.ReplayBuffer = null;
  }
};

class FLocalFileEventInfo
{
  //int32 ChunkIndex;
  //
  //FString Id;
  //FString Group;
  //FString Metadata;
  //uint32 Time1;
  //uint32 Time2;
  //
  //int32 SizeInBytes;
  //int64 EventDataOffset;

  constructor(){
    this.ChunkIndex = new TypedVal('uint32',INDEX_NONE);
    this.Id = new TypedVal('FString','');
    this.Group = new TypedVal('FString','');
    this.Metadata = new TypedVal('FString','');
    this.Time1 = new TypedVal('uint32',0);
    this.Time2 = new TypedVal('uint32',0);
    this.SizeInBytes = new TypedVal('uint32',0);
    this.EventDataOffset = new TypedVal('int64',0);
  }
}

class FLocalFileReplayInfo
{
  //int32 LengthInMS;
  //uint32 NetworkVersion;
  //uint32 Changelist;
  //FString FriendlyName;
  //FDateTime Timestamp;
  //int64 TotalDataSizeInBytes;
  //bool bIsLive;
  //bool bIsValid;
  //bool bCompressed;
  //
  //int32 HeaderChunkIndex;
  //
  //TArray<FLocalFileChunkInfo> Chunks;
  //TArray<FLocalFileEventInfo> Checkpoints;
  //TArray<FLocalFileEventInfo> Events;
  //TArray<FLocalFileReplayDataInfo> DataChunks;
  //
  constructor(){

    this.MagicNumber = new TypedVal('uint32',0);
    this.FileVersion = new TypedVal('uint32',0);
    this.LengthInMS = new TypedVal('int32',0);
    this.NetworkVersion = new TypedVal('uint32',0);
    this.Changelist = new TypedVal('uint32',0);
    this.FriendlyName = new TypedVal('FString','');
    this.Timestamp = new TypedVal('FDateTime',0);

    this.TotalDataSizeInBytes = 0;

    this.bIsLive = new TypedVal('bool',false);
    this.bIsValid = new TypedVal('bool',false);
    this.bCompressed = new TypedVal('bool',false);

    this.HeaderChunkIndex = INDEX_NONE;
    
    // not from UE
    this.Header = null;

    this.Chunks = [];
    this.Checkpoints = [];
    this.Events = [];
    this.DataChunks = [];
  }
}

class FLocalFileStreamFArchive extends FArchive
{
  constructor(filename,options){
    super(null,options);
    if (filename != undefined){
      this.FilePath = filename;
      this.Buffer = fs.readFileSync(this.FilePath);
    } else {
      this.Buffer = null;
    }
    this.Pos = 0;
  }
  SetBuffer(buf){
    this.Buffer = buf;
  }
}


class FLocalFileNetworkReplayStreamer extends EventEmitter
{
  constructor(DemoSavePath,ReplayFile){
    super();

    this.DemoSavePath = DemoSavePath;

    this.ReplayFile = ReplayFile;
    this.Info = null;

    this.ReplayAr = null; // will create when we start streaming.

   /** Handle to the archive that will read/write the demo header */
    this.HeaderAr = null;

    /** Handle to the archive that will read/write network packets */
    this.StreamAr = null;

    /* Handle to the archive that will read/write checkpoint files */
    this.CheckpointAr = null;
  }

  ReadReplay(ReplayBuffer){
    if (this.ReplayFile != undefined && this.ReplayFile != null)
      this.ReplayAr = new FLocalFileStreamFArchive(this.ReplayFile);
    else {
      this.ReplayAr = new FLocalFileStreamFArchive();
      this.ReplayAr.SetBuffer(ReplayBuffer);
    }
    return this.ReadReplayInfo(this.ReplayAr);
  }
  GetBasicInfo(){
    return {
      MagicNumber: this.Info.MagicNumber,
      FileVersion: this.Info.FileVersion,
      LengthInMS:  this.Info.LengthInMS,
      NetworkVersion: this.Info.NetworkVersion,
      Changelist: this.Info.Changelist,
      FriendlyName: this.Info.FriendlyName,
      Timestamp: this.Info.Timestamp,
      TotalDataSizeInBytes: this.Info.TotalDataSizeInBytes,
      bIsLive: this.Info.bIsLive,
      bIsValid: this.Info.bIsValid,
      bCompressed: this.Info.bComressed,
      HeaderChunkIndex: this.Info.HeaderChunkIndex
    };
  }
  GetInfo(){
    return this.Info;
  }
  GetHeaderArchive(){
    if (this.HeaderAr == null){
      let Chunk = this.Info.Chunks[this.Info.HeaderChunkIndex];
      //console.log(Chunk);
      this.HeaderAr = new FLocalFileStreamFArchive();
      this.HeaderAr.SetBuffer(
        this.ReplayAr.Buffer.slice(Chunk.DataOffset,Chunk.DataOffset+Chunk.SizeInBytes)
      );
      //console.log(this.HeaderAr.Buffer);
    }
    return this.HeaderAr;
  }
  GetCheckpointCount(){
    return this.Info.Checkpoints.length;
  }
  GetCheckpointArchive(i){
    if (this.Info.Checkpoints.IsValidIndex(i)){
      let Checkpoint = this.Info.Checkpoints[i];
      this.CheckpointAr = new FLocalFileStreamFArchive();
      if (this.Info.bCompressed){
        let InBuf = this.ReplayAr.Buffer.slice(Checkpoint.CompressedDataOffset, Checkpoint.CompressedDataOffset + Checkpoint.CompressedSize);
        let OutBuf = Buffer.alloc(Checkpoint.DecompressedSize);
        Decompress(InBuf,OutBuf);
        //fs.writeFileSync(Checkpoint.Id,OutBuf);
        this.CheckpointAr.SetBuffer(OutBuf);
      } else {
        this.CheckpointAr.SetBufer(
          this.ReplayAr.Buffer.slice(Checkpoint.EventDataOffset,Checkpoint.EventDataOffset+Checkpoint.SizeInBytes)
        );
      }
      return this.CheckpointAr;
    } else {
      return null;
    }
  }

  StartStreamingCurrentReplay(){
  }

  ReadReplayInfo(Archive){
    let Info = new FLocalFileReplayInfo();

    Archive.out(Info,'MagicNumber');
    Archive.out(Info,'FileVersion');

    if (Info.MagicNumber == FileMagic){

      Archive.out(Info,'LengthInMS');
      Archive.out(Info,'NetworkVersion');
      Archive.out(Info,'Changelist');
      Archive.out(Info,'FriendlyName');

      if (Info.FileVersion >= ELocalFileVersionHistory.HISTORY_FIXEDSIZE_FRIENDLY_NAME)
      {
        // trim whitespace since this may have been padded
        Info.FriendlyName = Info.FriendlyName.trim();
      }

      Archive.out(Info,'bIsLive');

      if (Info.FileVersion >= ELocalFileVersionHistory.HISTORY_RECORDED_TIMESTAMP)
      {
        Archive.out(Info,'Timestamp');
      }

      if (Info.FileVersion >= ELocalFileVersionHistory.HISTORY_COMPRESSION)
      {
        Archive.out(Info,'bCompressed');
      }

      // now look for all chunks
      while (!Archive.AtEnd())
      {
        let TypeOffset = Archive.Tell();

        let Chunk = new FLocalFileChunkInfo();
        let Idx = Info.Chunks.length;
        Info.Chunks.push(Chunk);

        Archive.out(Chunk,'ChunkType');
        //console.log("ChunkType: " + Chunk.ChunkType);
        Archive.out(Chunk,'SizeInBytes');

        Chunk.TypeOffset = TypeOffset;
        Chunk.DataOffset = Archive.Tell();

        if ((Chunk.SizeInBytes < 0) || ((Chunk.DataOffset + Chunk.SizeInBytes) > Archive.TotalSize()))
        {
          throw new Error("ReadReplayInfo: Invalid chunk size: " + Chunk.SizeInBytes);
          Archive.SetError();
          return null;
        }
        switch(Chunk.ChunkType)
        {
          case ELocalFileChunkType.Header:
            {
              if (Info.HeaderChunkIndex == INDEX_NONE)
              {
                Info.HeaderChunkIndex = Idx;
              }
              else
              {
                throw new Error("ReadReplayInfo: Found multiple header chunks");
                Archive.SetError();
                return null;
              }
            }
            this.emit('Header',Archive,Info,Info.Header);
            break;
          case ELocalFileChunkType.Checkpoint:
            {
              let Checkpoint = new  FLocalFileEventInfo();
              Info.Checkpoints.push(Checkpoint);
              let CheckpointIdx = Info.Checkpoints.length;

              Checkpoint.ChunkIndex = Idx;

              Archive.out(Checkpoint,'Id');
              Archive.out(Checkpoint,'Group');
              Archive.out(Checkpoint,'Metadata');
              Archive.out(Checkpoint,'Time1');
              Archive.out(Checkpoint,'Time2');

              Archive.out(Checkpoint,'SizeInBytes');

              Checkpoint.EventDataOffset = Archive.Tell();

              if ((Checkpoint.SizeInBytes < 0) || ((Checkpoint.EventDataOffset + Checkpoint.SizeInBytes) > Archive.TotalSize()))
              {
                Archive.SetError();
                throw new Error("ReadReplayInfo: Invalid checkpoint size: " + Checkpoint.SizeInBytes);
                return null;
              }

              if (Info.bCompressed)
              {
                Checkpoint.DecompressedSize = new TypedVal('int32',0);
                Archive.out(Checkpoint,'DecompressedSize');
                Checkpoint.CompressedSize = new TypedVal('int32',0);
                Archive.out(Checkpoint,'CompressedSize');
                if (Checkpoint.DecompressedSize < 0)
                {
                  Archive.SetError();
                  throw ("ReadReplayInfo: Invalid decompressed checkpoint size: " + Checkpoint.DecompressedSize);
                  return false;
                }
                Checkpoint.CompressedDataOffset = Archive.Tell();
              }
              this.emit('Checkpoint',Archive,Info,Checkpoint);
            }
            break;
          case ELocalFileChunkType.ReplayData:
            {
              let DataChunk = new FLocalFileReplayDataInfo();
              Info.DataChunks.push(DataChunk);
              let DataIdx = Info.DataChunks.length;
              DataChunk.ChunkIndex = Idx;
              DataChunk.StreamOffset = Info.TotalDataSizeInBytes;

              if (Info.FileVersion >= ELocalFileVersionHistory.HISTORY_STREAM_CHUNK_TIMES)
              {
                Archive.out(DataChunk,'Time1');
                Archive.out(DataChunk,'Time2');
                Archive.out(DataChunk,'SizeInBytes');
              }
              else
              {
                DataChunk.SizeInBytes = Chunk.SizeInBytes;
              }

              DataChunk.ReplayDataOffset = Archive.Tell();

              if ((DataChunk.SizeInBytes < 0) || ((DataChunk.ReplayDataOffset + DataChunk.SizeInBytes) > Archive.TotalSize()))
              {
                throw new Error("ReadReplayInfo: Invalid stream chunk size: ", DataChunk.SizeInBytes);
                Archive.SetError();
                return null;
              }

              if (Info.bCompressed)
              {
                //int32 DecompressedSize = GetDecompressedSize(Archive);
                DataChunk.DecompressedSize = new TypedVal('int32',0);
                Archive.out(DataChunk,'DecompressedSize');
                DataChunk.CompressedSize = new TypedVal('int32',0);
                Archive.out(DataChunk,'CompressedSize');
                if (DataChunk.DecompressedSize < 0)
                {
                  Archive.SetError();
                  throw ("ReadReplayInfo: Invalid decompressed replay data size: " + DecompressedSize);
                  return false;
                }
                DataChunk.CompressedDataOffset = Archive.Tell();
                let InBuf = Archive.Buffer.slice(DataChunk.CompressedDataOffset, DataChunk.CompressedDataOffset + DataChunk.CompressedSize);
                let OutBuf = Buffer.alloc(DataChunk.DecompressedSize);
                Decompress(InBuf,OutBuf);
                DataChunk.ReplayBuffer = OutBuf;
                //fs.writeFileSync('replaydata_' + DataChunk.ChunkIndex,OutBuf);

                Info.TotalDataSizeInBytes += DataChunk.DecompressedSize;
              }
              else
              {
                DataChunk.ReplayBuffer = Archive.Buffer.slice(DataChunk.StreamOffset, DataChunk.StreamOffset + DataChunk.SizeInBytes);
                Info.TotalDataSizeInBytes += DataChunk.SizeInBytes;
              }
              this.emit('Replay',Archive,Info,DataChunk);
            }
            break;
          case ELocalFileChunkType.Event:
            {
              let Event = new FLocalFileEventInfo();
              Info.Events.push(Event);
              let EventIdx = Info.Events.length;
              Event.ChunkIndex = Idx;

              Archive.out(Event,'Id');
              Archive.out(Event,'Group');
              Archive.out(Event,'Metadata');
              Archive.out(Event,'Time1');
              Archive.out(Event,'Time2');

              Archive.out(Event,'SizeInBytes');

              Event.EventDataOffset = Archive.Tell();

              if ((Event.SizeInBytes < 0) || ((Event.EventDataOffset + Event.SizeInBytes) > Archive.TotalSize()))
              {
                throw new Error("ReadReplayInfo: Invalid event size: " + Event.SizeInBytes);
                Archive.SetError();
                return null;
              }
              this.emit('Event',Archive,Info,Event);
            }
            break;
          case ELocalFileChunkType.Unknown:
            process.emitWarning("ReadReplayInfo: Skipping unknown (cleared) chunk");
            break;
          default:
            process.emitWarning("ReadReplayInfo: Unhandled file chunk type: " + Chunk.ChunkType);
            break;
        }

        if (Archive.IsError())
        {
          throw new Error("ReadReplayInfo: Archive error after parsing chunk");
          return null;
        }

        Archive.Seek(Chunk.DataOffset + Chunk.SizeInBytes);
      }
    }

    if (Info.FileVersion < ELocalFileVersionHistory.HISTORY_STREAM_CHUNK_TIMES)
    {
      for(let i=0; i < Info.DataChunks.length; ++i)
      {
        let CheckpointStartIdx = i - 1;

        if (Info.Checkpoints.IsValidIndex(CheckpointStartIdx))
        {
          Info.DataChunks[i].Time1 = Info.Checkpoints[CheckpointStartIdx].Time1;
        }
        else
        {
          Info.DataChunks[i].Time1 = 0;
        }

        if (Info.Checkpoints.IsValidIndex(i))
        {
          Info.DataChunks[i].Time2 = Info.Checkpoints[i].Time1;
        }
        else
        {
          Info.DataChunks[i].Time2 = Info.LengthInMS;
        }
      }
    }

    Info.bIsValid = Info.Chunks.IsValidIndex(Info.HeaderChunkIndex);

    this.Info = Info;

    return (Info.bIsValid && !Archive.IsError())? true : false;
  }
}

module.exports = {
  'FLocalFileStreamFArchive': FLocalFileStreamFArchive,
  'FLocalFileNetworkReplayStreamer': FLocalFileNetworkReplayStreamer,
};
