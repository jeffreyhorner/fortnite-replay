"use strict";
const {UDemoNetDriver} = require('./Engine/Classes/Engine/DemoNetDriver.js');
const util = require('util');

if (process.argv[2] == undefined){
  console.log("Need a File");
  process.exit(0);
}
let DemoNetDriver = new UDemoNetDriver('.',process.argv[2]);
DemoNetDriver.InitConnection();
DemoNetDriver.ReadPlaybackDemoHeader();

//console.log(DemoNetDriver.ReplayStreamer.GetInfo());

//console.log(JSON.stringify(DemoNetDriver.GetDemoHeader()));
// console.log(DemoNetDriver.HasLevelStreamingFixes());

let CheckpointCount = DemoNetDriver.ReplayStreamer.GetCheckpointCount();
//let x = [];
for (let i = 0; i < CheckpointCount; i++){
  let CheckpointAr = DemoNetDriver.ReplayStreamer.GetCheckpointArchive(i);
  DemoNetDriver.LoadCheckpoint(CheckpointAr);
  console.log(DemoNetDriver.DemoNetObject);
  //console.log(JSON.stringify(DemoNetDriver.PackageMapClient));
  //x.push(DemoNetDriver.PackageMapClient.NetFieldExportGroupMap);
}
//console.log(x);
