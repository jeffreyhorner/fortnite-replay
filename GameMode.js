"use strict";
const {UDemoNetDriver} = require('./Engine/Classes/Engine/DemoNetDriver.js');
const {FindPlaylist}  = require('./Game/Fortnite/Playlist.js');
const util = require('util');

if (process.argv[2] == undefined){
  console.log("Need a File");
  process.exit(0);
}
let DemoNetDriver = new UDemoNetDriver('.',process.argv[2]);
DemoNetDriver.InitConnection();
DemoNetDriver.ReadPlaybackDemoHeader();
let info = DemoNetDriver.ReplayStreamer.GetInfo();
let datachunks = info.DataChunks;
let playlist = null;
for (let i = 0; i < datachunks.length; i++){
  playlist = FindPlaylist(datachunks[i].ReplayBuffer);
  if (playlist != null) break;
}
console.log(playlist);
