"use strict";

const {TicksToUnixTimestamp} = require('../../../build/Release/FortniteReplayModule');
const iconv = require('iconv-lite');
const {Int64} = require('int64_t');

class TypedVal {
  constructor(type,val,sub_type_obj){
    this.type = type;
    this.val = val;
    this.beginPos = 0;
    this.endPos = 0;
    if (sub_type_obj != undefined){
      this.sub_type_obj = sub_type_obj
    }
  }
}

class FArchive
{
  constructor(buf,options){
    this.Pos = 0;
    this.Buffer = null
    this.ArchiveError = false;

    if (buf != undefined)
      this.Buffer = buf;

    // options
    this.options = {
      saveTypedVals: false,
      debug: false
    };
    if (typeof(options) === 'object'){
      if ('saveTypedVals' in options) this.options.saveTypedVals = options.saveTypedVals
      if ('debug' in options) this.options.debug = options.debug
    }

    //futher customization
    if (this.options.saveTypedVals == true){
      this.saveTypedVals = [];
    }
  }

  IsError(){ return this.ArchiveError == true; }

  SetError(){
    this.ArchiveError = true;
  }

  Serialize( Buffer, Length ){}

  Tell() { return this.Pos; }

  TotalSize() {
    if (this.Buffer === null)
      return 0;
    else
      return this.Buffer.byteLength;
  }

  Seek( InPos ){
    if (InPos <= this.Buffer.byteLength)
      this.Pos = InPos;
  }

  AtEnd(){
    if (this.Buffer === null) return false;
    return this.Pos >= this.Buffer.byteLength ; // && this.bAtEndOfReplay;
  }

  debugString(){
    return "Archive at " + this.Pos + " + buffer: " + this.Buffer.slice(this.Pos,this.Pos+32).toString('hex')
  }

  UnSerializeIntPacked(){
    let Value = 0;
    let cnt = 0;
    let more = 1;
    let iter = 0;
    while(more == 1)
    {
      iter++;
      // Serialize(&NextByte, 1);      // Read next byte
      let NextByte = this.Buffer.readUInt8(this.Pos);
      this.Pos += 1;

      more = NextByte & 1;        // Check 1 bit to see if theres more after this
      NextByte = NextByte >> 1;     // Shift to get actual 7 bit value
      Value += NextByte << (7 * cnt++); // Add to total value
    }
    return Value;
  }

  // Unserialize object property.
  // we use the fact that javascript passes references to objects.
  // assume call by ref.
  out(obj,prop){
    let array_length = null;
    let sub_obj = null;
    let i = 0;

    if ((prop in obj) == false) throw new Error('obj has no property named ' + prop);

    if (obj[prop].constructor.name != 'TypedVal') throw new Error('obj is not a TypedVal');

    switch(obj[prop].type){

      // Dangerous!
      case 'int64':
        let buf = this.Buffer.slice(this.Pos,this.Pos+8);
        obj[prop].beginPos = this.Pos;
        obj[prop].val = Number(new Int64(buf.swap64()));
        this.Pos += 8;
        obj[prop].endPos = this.Pos;
      break;

      case 'float':
        obj[prop].val = this.Buffer.readFloatLE(this.Pos);
        obj[prop].beginPos = this.Pos;
        this.Pos += 4;
        obj[prop].endPos = this.Pos;
      break;

      case 'uint8':
        obj[prop].val = this.Buffer.readUInt8(this.Pos);
        obj[prop].beginPos = this.Pos;
        this.Pos += 1;
        obj[prop].endPos = this.Pos;
      break;

      case 'uint16':
        obj[prop].val = this.Buffer.readUInt16LE(this.Pos);
        obj[prop].beginPos = this.Pos;
        this.Pos += 2;
        obj[prop].endPos = this.Pos;
      break;

      case 'uint32':
      case 'bool':
        obj[prop].val = this.Buffer.readUInt32LE(this.Pos);
        obj[prop].beginPos = this.Pos;
        this.Pos += 4;
        obj[prop].endPos = this.Pos;
        if (obj[prop].type == 'bool') obj[prop].val = (obj[prop].val == 1)? true: false;
      break;
      
      case 'int32':
        obj[prop].val = this.Buffer.readInt32LE(this.Pos);
        obj[prop].beginPos = this.Pos;
        this.Pos += 4;
        obj[prop].endPos = this.Pos;
      break;

      case 'FString':
        let size = this.Buffer.readInt32LE(this.Pos);
        obj[prop].beginPos = this.Pos;
        this.Pos += 4;
        if (size == 0 || size == 1){
          obj[prop].val = ''
          obj[prop].endPos = this.Pos;
        }
        else
        {
          let ucs2check = false;
          if (size < 0){
            ucs2check = true;
            size = -size;
          }

          if (size < 0){
            throw new Error("Corrupt FString!");
          }

          if (ucs2check == true){
            size *= 2;
            obj[prop].val = iconv.decode(this.Buffer.slice(this.Pos,this.Pos + size - 1),'UCS2');
          } else {
            obj[prop].val = this.Buffer.toString('utf8', this.Pos, this.Pos + size - 1);
          }
          this.Pos += size;
          obj[prop].endPos = this.Pos;
        }
        break;

      case 'FDateTime':
        obj[prop].val =  TicksToUnixTimestamp(this.Buffer.slice(this.Pos,this.Pos+8));
        obj[prop].date = new Date(obj[prop].val * 1000);
        obj[prop].datestring = obj[prop].date.toISOString();
        obj[prop].beginPos = this.Pos;
        this.Pos += 8 ;
        obj[prop].endPos = this.Pos;
      break;

      case 'TArray':
      case 'TArray<IntPacked>':
        obj[prop].beginPos = this.Pos;
        if (obj[prop].type=='TArray'){
          array_length = this.Buffer.readInt32LE(this.Pos);
          this.Pos += 4;
        } else {
          array_length = this.UnSerializeIntPacked();
        }

        obj[prop].val = [];
        sub_obj = obj[prop].sub_type_obj;
        for (i = 0; i < array_length; i++){
          let new_elem = Object.assign(Object.create( Object.getPrototypeOf(sub_obj)), sub_obj);
          new_elem.out(this);
          obj[prop].val.push(new_elem);
        }
        obj[prop].endPos = this.Pos;
      break;

      case 'TSet':
        obj[prop].beginPos = this.Pos;
        array_length = this.UnSerializeIntPacked();

        obj[prop].val = [];
        sub_obj = obj[prop].sub_type_obj;
        for (i = 0; i < array_length; i++){
          let new_elem = Object.assign(Object.create( Object.getPrototypeOf(sub_obj)), sub_obj);
          new_elem.out(this);
          obj[prop].val.push(new_elem);
        }
        obj[prop].endPos = this.Pos;
      break;

      case 'intPacked':
        obj[prop].beginPos = this.Pos;
        obj[prop].val = this.UnSerializeIntPacked();
        obj[prop].endPos = this.Pos;
      break;

      default:
        throw new Error('obj[prop] type ' + obj[prop].type + ' unreadable');
    }

    if (this.options.debug==true){
      console.log(obj[prop].val + ' ' + obj[prop].type + ' ' + obj[prop].beginPos + ' ' + obj[prop].endPos);
    }

    obj[prop] = obj[prop].val

    return obj[prop]
  }
}
exports.TypedVal = TypedVal;
exports.FArchive = FArchive;
