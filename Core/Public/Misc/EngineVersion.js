const {TypedVal, FArchive} = require('../../../Core/Public/Serialization/Archive.js');

class FEngineVersion
{
  constructor(){
  /** Major version number. */
    //uint16 Major;
    this.Major = new TypedVal('uint16',0);

  /** Minor version number. */
    //uint16 Minor;
    this.Minor = new TypedVal('uint16',0);

  /** Patch version number. */
    //uint16 Patch;
    this.Patch = new TypedVal('uint16',0);

  /** Changelist number. This is used to arbitrate when Major/Minor/Patch version numbers match. Use GetChangelist() instead of using this member directly. */
    //uint32 Changelist;
    this.Changelist = new TypedVal('uint32',0);

    // Possible.
    this.Branch = new TypedVal('FString','');
  }
  out(Ar){
    Ar.out(this,'Major');
    Ar.out(this,'Minor');
    Ar.out(this,'Patch');
    Ar.out(this,'Changelist');
    Ar.out(this,'Branch');
  }
}
exports.FEngineVersion = FEngineVersion;
