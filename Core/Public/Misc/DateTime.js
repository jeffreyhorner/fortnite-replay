"use strict";

const int64 = require('int64_t');

const DaysPerMonth = [ 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
const DaysToMonth = [ 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 ];


/** The number of timespan ticks per day: 864000000000 */
const TicksPerDay = new int64.Int64(0xc9,0x2a69c000);

/** The number of timespan ticks per hour: 36000000000 */
const TicksPerHour = new int64.Int64(0x8,0x61c46800);

/** The number of timespan ticks per microsecond: 10 */
const TicksPerMicrosecond = new int64.Int64(0x0,10);

/** The number of timespan ticks per millisecond. */
const TicksPerMillisecond = new int64.Int64(0,10000);

/** The number of timespan ticks per minute. */
const TicksPerMinute = new int64.Int64(0,600000000);

/** The number of timespan ticks per second. */
const TicksPerSecond = new int64.Int64(0,10000000);

function IsLeapYear(Year)
{
  if ((Year % 4) == 0)
  {
    return (((Year % 100) != 0) || ((Year % 400) == 0));
  }

  return false;
}

function DateTime(Year, Month, Day, Hour, Minute, Second, Millisecond)
{
  let TotalDays = 0;

  if ((Month > 2) && IsLeapYear(Year))
  {
    ++TotalDays;
  }

  --Year;                     // the current year is not a full year yet
  --Month;                    // the current month is not a full month yet

  TotalDays += Year * 365;
  TotalDays += Year / 4;              // leap year day every four years...
  TotalDays -= Year / 100;            // ...except every 100 years...
  TotalDays += Year / 400;            // ...but also every 400 years
  TotalDays += DaysToMonth[Month];        // days in this year up to last month
  TotalDays += Day - 1;             // days in this month minus today

  Ticks = TotalDays * TicksPerDay
    + Hour * TicksPerHour
    + Minute * TicksPerMinute
    + Second * TicksPerSecond
    + Millisecond * TicksPerMillisecond;

  return Ticks;
}

function GetHour(Ticks)
{
  return ((Ticks / TicksPerHour) % 24);
}

function GetMinute(Ticks)
{
  return ((Ticks / TicksPerMinute) % 60);
}

function UnixTimestampFromTicks(Ticks){
  return Ticks.sub(DateTime(1970, 1, 1,0,0,0,0)) / TicksPerSecond;
}

function UnixTimestampFromBuffer(buf){
  // console.log(buf);
  let Ticks = new int64.Int64(buf.swap64());
  return Ticks.sub(DateTime(1970, 1, 1,0,0,0,0)) / TicksPerSecond;
}

module.exports = {
  'DateTime': DateTime,
  'UnixTimestampFromTicks': UnixTimestampFromTicks,
  'UnixTimestampFromBuffer': UnixTimestampFromBuffer
};
