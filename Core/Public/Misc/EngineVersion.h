const {TypedVal, FArchive} = require('../../../Core/Public/Serialization/Archive.js');

class FEngineVersion
{
  constructor(){
  /** Major version number. */
    uint16 Major;

  /** Minor version number. */
    uint16 Minor;

  /** Patch version number. */
    uint16 Patch;

  /** Changelist number. This is used to arbitrate when Major/Minor/Patch version numbers match. Use GetChangelist() instead of using this member directly. */
    uint32 Changelist;

  }
  out(Ar){
  }
}
