"use strict";
const fs = require('fs');
const util = require('util');

if (process.argv[2] == undefined){
  console.log("Need a File");
  process.exit(0);
}
let Data = fs.readFileSync(process.argv[2]);
let Count = Data.length;
if (Count > 0)
{
  let LastByte = Data[Count-1];

  if (LastByte != 0)
  {
    let BitSize = (Count * 8) - 1;
    let OldBitSize = BitSize;

    // Bit streaming, starts at the Least Significant Bit, and ends at the MSB.
    while (!(LastByte & 0x80))
    {
      LastByte *= 2;
      BitSize--;
    }

    console.log('OldBitSize: ' + OldBitSize + ' BitSize: ' + BitSize + ' Count: ' + Count);


    //FBitReader Reader(Data, BitSize);

    // Set the network version on the reader
    //Reader.SetEngineNetVer( EngineNetworkProtocolVersion );
    //Reader.SetGameNetVer( GameNetworkProtocolVersion );

    //if (Handler.IsValid())
    //{
      //Handler->IncomingHigh(Reader);
    //}

    //if (Reader.GetBitsLeft() > 0)
    //{
      //ReceivedPacket(Reader);
    //}
  }
  // MalformedPacket - Received a packet with 0's in the last byte
  else
  {
    console.log('Bad Packet');
  }
}
