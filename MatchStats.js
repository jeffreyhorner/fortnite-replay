"use strict";
const {UDemoNetDriver} = require('./Engine/Classes/Engine/DemoNetDriver.js');
const {AthenaMatchStats} = require('./Game/Fortnite/AthenaMatchStats.js');
const {AthenaMatchTeamStats} = require('./Game/Fortnite/AthenaMatchTeamStats.js');
const util = require('util');

if (process.argv[2] == undefined){
  console.log("Need a File");
  process.exit(0);
}
let DemoNetDriver = new UDemoNetDriver('.',process.argv[2]);
DemoNetDriver.InitConnection();
let replayAr = DemoNetDriver.ReplayStreamer.ReplayAr;
let info = DemoNetDriver.ReplayStreamer.Info;
let events = info.Events;
let elims = [];
let elim = null;
for (let i = 0; i < events.length; i++){
  if (events[i].Metadata=='AthenaMatchStats'){
    elim = new AthenaMatchStats();
    elim.out(replayAr,info,events[i]);
    elims.push(elim);
  } else if (events[i].Metadata=='AthenaMatchTeamStats'){
    elim = new AthenaMatchTeamStats();
    elim.out(replayAr,info,events[i]);
    elims.push(elim);
  }
}
console.log(JSON.stringify(elims));
