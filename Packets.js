"use strict";
const {UDemoNetDriver} = require('./Engine/Classes/Engine/DemoNetDriver.js');
const {PlayerStateEncryptionKey} = require('./Game/Fortnite/PlayerStateEncryptionKey.js');
const fs = require('fs');
const util = require('util');

if (process.argv[2] == undefined){
  console.log("Need a File");
  process.exit(0);
}

let DemoNetDriver = new UDemoNetDriver('.',process.argv[2]);
DemoNetDriver.InitConnection();
DemoNetDriver.ReadPlaybackDemoHeader();
let CheckpointCount = DemoNetDriver.ReplayStreamer.GetCheckpointCount();
let i =0;
for (i = 0; i < CheckpointCount; i++){
  let CheckpointAr = DemoNetDriver.ReplayStreamer.GetCheckpointArchive(i);
  DemoNetDriver.LoadCheckpoint(CheckpointAr);
  //console.log(DemoNetDriver.DemoNetObject);
  //console.log(JSON.stringify(DemoNetDriver.PackageMapClient));
}

let Packets = DemoNetDriver.Packets;
for (i = 0; i < Packets.length; i++){
  fs.writeFileSync('packet_' + i,Packets[i].buffer);
}
console.log(JSON.stringify(DemoNetDriver.Packets));
