﻿# Open port 3000 
#New-NetFirewallRule -DisplayName "fortnite-replay" -Profile @('Any') -Direction Inbound -Action Allow -Protocol TCP -LocalPort 3000

# Compression library 
Copy-Item .\oo2core_5_win64.dll C:\Windows\System32\.

# Uninstall any older version
.\node .\WindowsService.js uninstall


# Install fortnite-replay-service
.\node .\WindowsService.js install

# Start service, should be started already
Start-Service fortnite-replay-service
