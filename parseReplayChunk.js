"use strict";

const {TypedVal, FArchive} = require('./Core/Public/Serialization/Archive.js');
const 
{
  FNetGUIDCacheObject,
  FNetFieldExport,
  FNetFieldExportGroup,
  UPackageMapClient
} = require('./Engine/Classes/Engine/PackageMapClient.js');
const fs = require('fs');
const util = require('util');
if (process.argv[2] == undefined){
  console.log("Need a File");
  process.exit(0);
}

let buf = fs.readFileSync(process.argv[2]);
let Ar = new FArchive(buf,{debug: true});
let NetFieldExportGroup = new FNetFieldExportGroup();

Ar.Seek(11);
console.log(Ar.debugString());


NetFieldExportGroup.out(Ar);
console.log(NetFieldExportGroup);
console.log('End: ' + Ar.Tell());

//Ar.Seek(5);

//NetFieldExportGroup.out(Ar);
//console.log(NetFieldExportGroup);
//console.log('End: ' + Ar.Tell());
