﻿# Be sure to run this within the fortnite-replay folder
git clone git@gitlab.com:jeffreyhorner/fortnite-replay.git ..\fortnite-replay-deploy
Push-Location -Path ..\fortnite-replay-deploy
Copy-Item $Env:OODLE_LIB .
npm install
Copy-Item $Env:NODE_BINARY .
Compress-Archive -DestinationPath ..\fortnite-replay-deploy.zip -Path . -Force
Pop-Location
