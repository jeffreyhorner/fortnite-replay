"use strict";
const {UDemoNetDriver} = require('./Engine/Classes/Engine/DemoNetDriver.js');
const {PlayerStateEncryptionKey} = require('./Game/Fortnite/PlayerStateEncryptionKey.js');
const util = require('util');

if (process.argv[2] == undefined){
  console.log("Need a File");
  process.exit(0);
}

let DemoNetDriver = new UDemoNetDriver('.',process.argv[2]);
DemoNetDriver.InitConnection();
DemoNetDriver.ReadPlaybackDemoHeader();
let CheckpointCount = DemoNetDriver.ReplayStreamer.GetCheckpointCount();
for (let i = 0; i < CheckpointCount; i++){
  let CheckpointAr = DemoNetDriver.ReplayStreamer.GetCheckpointArchive(i);
  DemoNetDriver.LoadCheckpoint(CheckpointAr);
  //console.log(DemoNetDriver.DemoNetObject);
  //console.log(JSON.stringify(DemoNetDriver.PackageMapClient));
}

let replayAr = DemoNetDriver.ReplayStreamer.ReplayAr;
let info = DemoNetDriver.ReplayStreamer.Info;
let events = info.Events;
let keys = [];
for (let i = 0; i < events.length; i++){
  if (events[i].Group=='PlayerStateEncryptionKey'){
    let key = new PlayerStateEncryptionKey();
    key.out(replayAr,info,events[i]);
    keys.push(key);
  }
}
console.log(JSON.stringify(keys));
